/* mtypes for strings */
mtype = { m_ready};
mtype = { m_init_cl};
mtype = { m_kill_cl};
mtype = { m_init_terminal};
/* inline to print mtype: strings or not */
inline print_mtype(var){
    if
            :: var == m_init_cl -> printf("init_cl\n");            
            :: var == m_kill_cl -> printf("kill_cl\n");            
            :: var == m_ready -> printf("ready\n");            
            :: var == m_init_terminal -> printf("init_terminal\n");            
            :: else ->  printf("%e\n", var)
    fi;
}
/* ++ globals_for_instances ++ */
#define SELF (_pid)
#define PId byte
#define TRUE (1)
#define FALSE (0)
#define Null (255)
#define OR ||
#define AND &&
#define PROC_LIMIT 25
#define INT_ARRAY_INDEX 25

#define NULL (0)


bool PROC_INIT[PROC_LIMIT]; /* set to 255 later  */
byte PARENT[PROC_LIMIT] = 255; /* set size to 255 later  */ /* PARENT[pid] == 255 means 'no parent defined' */

/* index is proc_NAME, value is number of instances */
byte MAX_PROC_COUNT[PROC_LIMIT] = 0;
byte CUR_PROC_COUNT[PROC_LIMIT] = 0;

mtype= { proc_buffer, proc_queue, proc_server, proc_client, proc_terminal };;
/* index is PId of existing process, value is proc_NAME (the 'proc_type') */
byte PROC_TYPE[PROC_LIMIT] = 0;
 /* block::out begin (name=Client_and_Terminals) (is_top=1) (1) */ 
/* executional_specification::type_definitions(block=HASH(0x55d26c0ec400), 1 0): */
 /* type_definition::out begin */ 
/* TYPE def: int_arr__revealed IS integer ARRAY OF integer */
 /* type_definition::out end */ 
 /* type_definition::out begin */ 
/* TYPE def: integer__revealed IS PId ARRAY OF integer */
 /* type_definition::out end */ 
 /* type_definition::out begin */ 
/* TYPE def: arr_int IS integer ARRAY OF integer */
 /* type_definition::out end */ 
/* executional_specification::type_definitions(subblocks=HASH(0x55d26c0ec2e0), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(executional_specification=HASH(0x55d26bce93e0), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(process=HASH(0x55d26bce91e8), 0 0): */
/* executional_specification::type_definitions(process_head=HASH(0x55d26bc070c8), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(executional_specification=HASH(0x55d26bda4dd8), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(process=HASH(0x55d26bda43d0), 0 0): */
 /* type_definition::out begin */ 
/* TYPE def: integer_array IS integer ARRAY OF integer */
 /* type_definition::out end */ 
 /* type_definition::out begin */ 
/* TYPE def: char_array IS integer ARRAY OF STR */
 /* type_definition::out end */ 
 /* type_definition::out begin */ 
/* TYPE def: PId_array IS integer ARRAY OF PId */
 /* type_definition::out end */ 
 /* type_definition::out begin */ 
/* TYPE def: arr_pid IS PId ARRAY OF integer */
 /* type_definition::out end */ 
 /* type_definition::out begin */ 
/* TYPE def: arr IS integer ARRAY OF PId */
 /* type_definition::out end */ 
/* executional_specification::type_definitions(process_head=HASH(0x55d26bce96e0), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(executional_specification=HASH(0x55d26bdf7158), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(process=HASH(0x55d26bdf6fa8), 0 0): */
 /* type_definition::out begin */ 
/* TYPE def: int_arr IS integer ARRAY OF integer */
 /* type_definition::out end */ 
/* executional_specification::type_definitions(process_head=HASH(0x55d26bda5090), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(executional_specification=HASH(0x55d26bf45ac8), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(process=HASH(0x55d26bf45918), 0 0): */
/* executional_specification::type_definitions(process_head_8=HASH(0x55d26bdf80a0), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(executional_specification=HASH(0x55d26c0ec1f0), 0 0): */
/* NO CONTEXT */
/* executional_specification::type_definitions(process=HASH(0x55d26c0ec040), 0 0): */
/* executional_specification::type_definitions(process_head_8=HASH(0x55d26bf47538), 0 0): */
/* NO CONTEXT */
 /* block::out mtypes= (S_balance S_card S_check_code S_check_summ S_cheque S_code S_correct_card S_correct_code S_done S_get_balance S_get_money S_init S_init_cl S_init_terminal S_insert_money S_insert_summ S_kill_cl S_limit S_money S_no_money S_press_start S_ready S_ready_for_job S_summ S_terminal_no_money S_wrong_card S_wrong_code) */ 
mtype = { S_balance, S_card, S_check_code, S_check_summ, S_cheque, S_code, S_correct_card, S_correct_code, S_done, S_get_balance, S_get_money, S_init, S_init_cl, S_init_terminal, S_insert_money, S_insert_summ, S_kill_cl, S_limit, S_money, S_no_money, S_press_start, S_ready, S_ready_for_job, S_summ, S_terminal_no_money, S_wrong_card, S_wrong_code};
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_ready S_init_terminal) */ 
chan C_terminal2queue_ch = [10] of { mtype, byte, PId } /* (param_count=1) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_summ S_no_money S_correct_code S_wrong_code) */ 
chan C_server2terminal_ch = [10] of { mtype, byte, PId, PId } /* (param_count=2) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_check_code S_check_summ S_balance S_insert_summ) */ 
chan C_buffer2server_ch = [10] of { mtype, byte, int, int, PId } /* (param_count=3) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_check_code S_check_summ S_balance S_insert_summ) */ 
chan C_terminal2server_ch = [10] of { mtype, byte, int, int, PId } /* (param_count=3) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_no_money S_correct_code S_wrong_code S_code S_summ S_done S_get_balance S_get_money S_insert_money S_press_start S_card) */ 
chan CV_buffer2terminal_ch[PROC_LIMIT] ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_card S_press_start S_insert_money S_get_money S_get_balance S_done S_summ S_code) */ 
chan C_client2terminal_ch = [10] of { mtype, byte, int, PId } /* (param_count=2) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_card S_terminal_no_money S_no_money S_correct_card S_limit S_correct_code S_wrong_card S_wrong_code S_ready_for_job S_cheque S_money) */ 
chan C_terminal2client_ch = [10] of { mtype, byte, int, PId } /* (param_count=2) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_kill_cl) */ 
chan C_client2queue_ch = [10] of { mtype, byte, PId } /* (param_count=1) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_card S_terminal_no_money S_no_money S_correct_card S_limit S_correct_code S_wrong_card S_wrong_code S_ready_for_job S_init S_cheque S_money) */ 
chan CV_buffer2client_ch[PROC_LIMIT] ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_init) */ 
chan C_queue2client_ch = [10] of { mtype, byte, int, int, int, PId, int, PId } /* (param_count=6) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_ready S_init_terminal S_init_cl S_kill_cl) */ 
chan C_buffer2queue_ch = [10] of { mtype, byte, PId, int, int, int, PId } /* (param_count=5) */ ; 
 /* channel_declaration::out end */ 
 /* channel_declaration::out begin */ 
 /* SIG NAMES = (S_init_cl) */ 
chan C_ENV2queue_ch = [10] of { mtype, byte, int, int, int, int, PId } /* (param_count=5) */ ; 
 /* channel_declaration::out end */ 
/* BLOCK VARS begin (name=Client_and_Terminals)*/ 
/* code_table OF int_arr__revealed */ 
int code_table[INT_ARRAY_INDEX]; 
/* summ_table OF int_arr__revealed */ 
int summ_table[INT_ARRAY_INDEX]; 
/* wants_summ OF integer__revealed */ 
int wants_summ[INT_ARRAY_INDEX]; 
/* gotten_summ OF integer__revealed */ 
int gotten_summ[INT_ARRAY_INDEX]; 
/* BLOCK VARS end */ 
/* -- globals_for_instances -- already made */
/* Processing PROCESS buffer */
/* ----------------------                 BEGIN PROCESS 'buffer'   ---------------- */
active proctype buffer() { /* BEGIN PROCESS buffer */ 
  /* process_local_vars_channels (..., ) */
  /* ++ locals_for_instances ++ */
  /* Needed for creation only */
  local byte aOFFSPRING; 
  local byte aSENDER; 
  local byte aPARENT; 
  
  
  CUR_PROC_COUNT[ proc_buffer ]++;
  
  
  /* Needed for time intervals */
  local int startTick; 
  /* ^^ locals_for_instances ^^ */
  /* process_local_vars_channels (..., AFTER VARS) */
/* p4_integer OF integer */ 
int p4_integer; 
/* p5_integer OF integer */ 
int p5_integer; 
/* p6_integer OF integer */ 
int p6_integer; 
/* p4_PId OF PId */ 
PId p4_PId; 
/* p3_integer OF integer */ 
int p3_integer; 
/* p2_integer OF integer */ 
int p2_integer; 
/* p1_integer OF integer */ 
int p1_integer; 
/* TARGET_PID OF PId */ 
PId TARGET_PID; 
L_read_signal : 
atomic{
do
  :: (C_terminal2queue_ch?[S_ready,_,_]) -> 
     C_terminal2queue_ch ? S_ready  (aSENDER, TARGET_PID); 

  goto L_write_ready_queue_ch;

  :: (C_terminal2queue_ch?[S_init_terminal,_,_]) -> 
     C_terminal2queue_ch ? S_init_terminal  (aSENDER, TARGET_PID); 

  goto L_write_init_terminal_queue_ch;

  :: (C_server2terminal_ch?[S_summ,_,_,_]) -> 
     C_server2terminal_ch ? S_summ  (aSENDER, p1_integer, TARGET_PID); 

  goto L_write_summ_terminal_ch;

  :: (C_server2terminal_ch?[S_no_money,_,_,_]) -> 
     C_server2terminal_ch ? S_no_money  (aSENDER, TARGET_PID, _); 

  goto L_write_no_money_terminal_ch;

  :: (C_server2terminal_ch?[S_correct_code,_,_,_]) -> 
     C_server2terminal_ch ? S_correct_code  (aSENDER, TARGET_PID, _); 

  goto L_write_correct_code_terminal_ch;

  :: (C_server2terminal_ch?[S_wrong_code,_,_,_]) -> 
     C_server2terminal_ch ? S_wrong_code  (aSENDER, TARGET_PID, _); 

  goto L_write_wrong_code_terminal_ch;

  :: (C_terminal2server_ch?[S_check_code,_,_,_,_]) -> 
     C_terminal2server_ch ? S_check_code  (aSENDER, p1_integer, p2_integer, TARGET_PID); 

  goto L_write_check_code_server_ch;

  :: (C_terminal2server_ch?[S_check_summ,_,_,_,_]) -> 
     C_terminal2server_ch ? S_check_summ  (aSENDER, p1_integer, p2_integer, TARGET_PID); 

  goto L_write_check_summ_server_ch;

  :: (C_terminal2server_ch?[S_balance,_,_,_,_]) -> 
     C_terminal2server_ch ? S_balance  (aSENDER, p1_integer, TARGET_PID, _); 

  goto L_write_balance_server_ch;

  :: (C_terminal2server_ch?[S_insert_summ,_,_,_,_]) -> 
     C_terminal2server_ch ? S_insert_summ  (aSENDER, p1_integer, p2_integer, TARGET_PID); 

  goto L_write_insert_summ_server_ch;

  :: (C_client2terminal_ch?[S_card,_,_,_]) -> 
     C_client2terminal_ch ? S_card  (aSENDER, p1_integer, TARGET_PID); 

  goto L_write_card_terminal_ch;

  :: (C_client2terminal_ch?[S_press_start,_,_,_]) -> 
     C_client2terminal_ch ? S_press_start  (aSENDER, TARGET_PID, _); 

  goto L_write_press_start_terminal_ch;

  :: (C_client2terminal_ch?[S_insert_money,_,_,_]) -> 
     C_client2terminal_ch ? S_insert_money  (aSENDER, TARGET_PID, _); 

  goto L_write_insert_money_terminal_ch;

  :: (C_client2terminal_ch?[S_get_money,_,_,_]) -> 
     C_client2terminal_ch ? S_get_money  (aSENDER, TARGET_PID, _); 

  goto L_write_get_money_terminal_ch;

  :: (C_client2terminal_ch?[S_get_balance,_,_,_]) -> 
     C_client2terminal_ch ? S_get_balance  (aSENDER, TARGET_PID, _); 

  goto L_write_get_balance_terminal_ch;

  :: (C_client2terminal_ch?[S_done,_,_,_]) -> 
     C_client2terminal_ch ? S_done  (aSENDER, TARGET_PID, _); 

  goto L_write_done_terminal_ch;

  :: (C_client2terminal_ch?[S_summ,_,_,_]) -> 
     C_client2terminal_ch ? S_summ  (aSENDER, p1_integer, TARGET_PID); 

  goto L_write_summ_terminal_ch;

  :: (C_client2terminal_ch?[S_code,_,_,_]) -> 
     C_client2terminal_ch ? S_code  (aSENDER, p1_integer, TARGET_PID); 

  goto L_write_code_terminal_ch;

  :: (C_terminal2client_ch?[S_card,_,_,_]) -> 
     C_terminal2client_ch ? S_card  (aSENDER, p1_integer, TARGET_PID); 

  goto L_write_card_client_ch;

  :: (C_terminal2client_ch?[S_terminal_no_money,_,_,_]) -> 
     C_terminal2client_ch ? S_terminal_no_money  (aSENDER, TARGET_PID, _); 

  goto L_write_terminal_no_money_client_ch;

  :: (C_terminal2client_ch?[S_no_money,_,_,_]) -> 
     C_terminal2client_ch ? S_no_money  (aSENDER, TARGET_PID, _); 

  goto L_write_no_money_client_ch;

  :: (C_terminal2client_ch?[S_correct_card,_,_,_]) -> 
     C_terminal2client_ch ? S_correct_card  (aSENDER, TARGET_PID, _); 

  goto L_write_correct_card_client_ch;

  :: (C_terminal2client_ch?[S_limit,_,_,_]) -> 
     C_terminal2client_ch ? S_limit  (aSENDER, TARGET_PID, _); 

  goto L_write_limit_client_ch;

  :: (C_terminal2client_ch?[S_correct_code,_,_,_]) -> 
     C_terminal2client_ch ? S_correct_code  (aSENDER, TARGET_PID, _); 

  goto L_write_correct_code_client_ch;

  :: (C_terminal2client_ch?[S_wrong_card,_,_,_]) -> 
     C_terminal2client_ch ? S_wrong_card  (aSENDER, TARGET_PID, _); 

  goto L_write_wrong_card_client_ch;

  :: (C_terminal2client_ch?[S_wrong_code,_,_,_]) -> 
     C_terminal2client_ch ? S_wrong_code  (aSENDER, TARGET_PID, _); 

  goto L_write_wrong_code_client_ch;

  :: (C_terminal2client_ch?[S_ready_for_job,_,_,_]) -> 
     C_terminal2client_ch ? S_ready_for_job  (aSENDER, TARGET_PID, _); 

  goto L_write_ready_for_job_client_ch;

  :: (C_terminal2client_ch?[S_cheque,_,_,_]) -> 
     C_terminal2client_ch ? S_cheque  (aSENDER, TARGET_PID, _); 

  goto L_write_cheque_client_ch;

  :: (C_terminal2client_ch?[S_money,_,_,_]) -> 
     C_terminal2client_ch ? S_money  (aSENDER, p1_integer, TARGET_PID); 

  goto L_write_money_client_ch;

  :: (C_client2queue_ch?[S_kill_cl,_,_]) -> 
     C_client2queue_ch ? S_kill_cl  (aSENDER, TARGET_PID); 

  goto L_write_kill_cl_queue_ch;

  :: (C_queue2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     C_queue2client_ch ? S_init  (aSENDER, p1_integer, p2_integer, p3_integer, p4_PId, p5_integer, TARGET_PID); 

  goto L_write_init_client_ch;

  :: (C_ENV2queue_ch?[S_init_cl,_,_,_,_,_,_]) -> 
     C_ENV2queue_ch ? S_init_cl  (aSENDER, p1_integer, p2_integer, p3_integer, p4_integer, TARGET_PID); 

  goto L_write_init_cl_queue_ch;

od;
}/* atomic */

L_write_ready_queue_ch : 
atomic{
    printf("WRITE: C_buffer2queue_ch ! S_ready( _pid aSENDER 99 99 99 99 ) ;\n");
  C_buffer2queue_ch ! S_ready( _pid, aSENDER, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_init_terminal_queue_ch : 
atomic{
    printf("WRITE: C_buffer2queue_ch ! S_init_terminal( _pid aSENDER 99 99 99 99 ) ;\n");
  C_buffer2queue_ch ! S_init_terminal( _pid, aSENDER, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_summ_terminal_ch : 
atomic{
do
  :: /* no condition */ 
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_summ( _pid p1_integer aSENDER ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_summ( _pid, p1_integer, aSENDER ) ;

  goto L_read_signal;

  :: /* no condition */ 
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_summ( _pid p1_integer aSENDER ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_summ( _pid, p1_integer, aSENDER ) ;

  goto L_read_signal;

od;
}/* atomic */

L_write_no_money_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_no_money( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_no_money( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_correct_code_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_correct_code( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_correct_code( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_wrong_code_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_wrong_code( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_wrong_code( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_check_code_server_ch : 
atomic{
    printf("WRITE: C_buffer2server_ch ! S_check_code( _pid p1_integer p2_integer aSENDER ) ;\n");
  C_buffer2server_ch ! S_check_code( _pid, p1_integer, p2_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L_write_check_summ_server_ch : 
atomic{
    printf("WRITE: C_buffer2server_ch ! S_check_summ( _pid p1_integer p2_integer aSENDER ) ;\n");
  C_buffer2server_ch ! S_check_summ( _pid, p1_integer, p2_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L_write_balance_server_ch : 
atomic{
    printf("WRITE: C_buffer2server_ch ! S_balance( _pid p1_integer aSENDER 99 ) ;\n");
  C_buffer2server_ch ! S_balance( _pid, p1_integer, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_insert_summ_server_ch : 
atomic{
    printf("WRITE: C_buffer2server_ch ! S_insert_summ( _pid p1_integer p2_integer aSENDER ) ;\n");
  C_buffer2server_ch ! S_insert_summ( _pid, p1_integer, p2_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L_write_card_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_card( _pid p1_integer aSENDER ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_card( _pid, p1_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L_write_press_start_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_press_start( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_press_start( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_insert_money_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_insert_money( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_insert_money( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_get_money_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_get_money( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_get_money( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_get_balance_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_get_balance( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_get_balance( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_done_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_done( _pid aSENDER 99 ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_done( _pid, aSENDER, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_code_terminal_ch : 
atomic{
    printf("WRITE: CV_buffer2terminal_ch[TARGET_PID] ! S_code( _pid p1_integer aSENDER ) ;\n");
  CV_buffer2terminal_ch[TARGET_PID] ! S_code( _pid, p1_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L_write_card_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_card( _pid p1_integer aSENDER 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_card( _pid, p1_integer, aSENDER, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_terminal_no_money_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_terminal_no_money( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_terminal_no_money( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_no_money_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_no_money( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_no_money( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_correct_card_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_correct_card( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_correct_card( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_limit_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_limit( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_limit( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_correct_code_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_correct_code( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_correct_code( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_wrong_card_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_wrong_card( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_wrong_card( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_wrong_code_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_wrong_code( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_wrong_code( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_ready_for_job_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_ready_for_job( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_ready_for_job( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_cheque_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_cheque( _pid aSENDER 99 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_cheque( _pid, aSENDER, 99, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_money_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_money( _pid p1_integer aSENDER 99 99 99 99 ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_money( _pid, p1_integer, aSENDER, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_kill_cl_queue_ch : 
atomic{
    printf("WRITE: C_buffer2queue_ch ! S_kill_cl( _pid aSENDER 99 99 99 99 ) ;\n");
  C_buffer2queue_ch ! S_kill_cl( _pid, aSENDER, 99, 99, 99, 99 ) ;

  goto L_read_signal;

}/* atomic */

L_write_init_client_ch : 
atomic{
    printf("WRITE: CV_buffer2client_ch[TARGET_PID] ! S_init( _pid p1_integer p2_integer p3_integer p4_PId p5_integer aSENDER ) ;\n");
  CV_buffer2client_ch[TARGET_PID] ! S_init( _pid, p1_integer, p2_integer, p3_integer, p4_PId, p5_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L_write_init_cl_queue_ch : 
atomic{
    printf("WRITE: C_buffer2queue_ch ! S_init_cl( _pid p1_integer p2_integer p3_integer p4_integer aSENDER ) ;\n");
  C_buffer2queue_ch ! S_init_cl( _pid, p1_integer, p2_integer, p3_integer, p4_integer, aSENDER ) ;

  goto L_read_signal;

}/* atomic */

L___end_of_process__: printf("[END OF PROCESS buffer(%d) WITH PID %d]\n", proc_buffer, _pid);
              CUR_PROC_COUNT[ proc_buffer ]--;
              PROC_TYPE[ _pid ] = 0;
} /* END PROCESS buffer */ 

/* -- globals_for_instances -- already made */
/* Processing PROCESS queue */

inline add_signal(p1, p2, p3, p4){ d_step{ /* PROCEDURE add_signal(p1, p2, p3, p4)*/
_sender_list[_i_size] = SENDER_PID; 
_sig_p1[_i_size] = p1; 
_sig_p2[_i_size] = p2; 
_sig_p3[_i_size] = p3; 
_sig_p4[_i_size] = p4; 
_i_size = _i_size+1; 
}} /* END PROCEDURE */
/* Global var for procedure shift_arrays */
int last;

inline shift_arrays(size, current){ d_step{ /* PROCEDURE shift_arrays(size, current)*/
last = current; 
    /* while (line: 603) */
    do
      ::( ( last<size-1 ) ) ->  
        _signal_list[last] = _signal_list[last+1]; 
        _sender_list[last] = _sender_list[last+1]; 
        _sig_p1[last] = _sig_p1[last+1]; 
        _sig_p2[last] = _sig_p2[last+1]; 
        _sig_p3[last] = _sig_p3[last+1]; 
        _sig_p4[last] = _sig_p4[last+1]; 
        last = last+1; 
      ::else -> break;
    od;
size = size-1; 
_signal_list[size] = NULL; 
_sender_list[size] = NULL; 
_sig_p1[size] = 0; 
_sig_p2[size] = 0; 
_sig_p3[size] = 0; 
_sig_p4[size] = 0; 
/* zeroing local vars et the end */
last = 0;
}} /* END PROCEDURE */
/* ----------------------                 BEGIN PROCESS 'queue'   ---------------- */
active proctype queue() { /* BEGIN PROCESS queue */ 
  /* process_local_vars_channels (..., ) */
  /* ++ locals_for_instances ++ */
  /* Needed for creation only */
  local byte aOFFSPRING; 
  local byte aSENDER; 
  local byte aPARENT; 
  
  
  CUR_PROC_COUNT[ proc_queue ]++;
  
  
  /* Needed for time intervals */
  local int startTick; 
  /* ^^ locals_for_instances ^^ */
  /* process_local_vars_channels (..., AFTER VARS) */
/* SENDER_PID OF PId */ 
PId SENDER_PID; 
/* terminal_count OF integer */ 
int terminal_count; 
/* terminal_num OF integer */ 
int terminal_num; 
/* MAX_TERMINAL OF integer */ 
int MAX_TERMINAL; 
/* cl_summ OF integer */ 
int cl_summ; 
/* cl_card_num OF integer */ 
int cl_card_num; 
/* cl_code OF integer */ 
int cl_code; 
/* cl_operation OF integer */ 
int cl_operation; 
/* count OF integer */ 
int count; 
/* pid_to_idx OF arr_pid */ 
int pid_to_idx[INT_ARRAY_INDEX]; 
/* terminal_inst OF arr */ 
PId terminal_inst[INT_ARRAY_INDEX]; 
/* _sig_p1 OF integer_array */ 
int _sig_p1[INT_ARRAY_INDEX]; 
/* _sig_p2 OF integer_array */ 
int _sig_p2[INT_ARRAY_INDEX]; 
/* _sig_p3 OF integer_array */ 
int _sig_p3[INT_ARRAY_INDEX]; 
/* _sig_p4 OF integer_array */ 
int _sig_p4[INT_ARRAY_INDEX]; 
/* _sender_list OF PId_array */ 
PId _sender_list[INT_ARRAY_INDEX]; 
/* _signal_list OF char_array */ 
mtype _signal_list[INT_ARRAY_INDEX]; 
/* _i_size OF integer */ 
int _i_size; 
/* _i_current OF integer */ 
int _i_current; 
/* _temp_cl_summ OF integer */ 
int _temp_cl_summ; 
/* _temp_cl_card_num OF integer */ 
int _temp_cl_card_num; 
/* _temp_cl_code OF integer */ 
int _temp_cl_code; 
/* _temp_cl_operation OF integer */ 
int _temp_cl_operation; 
L_Start__rl : 
atomic{
   /* EXE (line: 622)  */ 
count = 0; 
terminal_count = 0; 
MAX_TERMINAL = 3; 
terminal_inst[1] = Null; 
terminal_inst[2] = Null; 
terminal_inst[3] = Null; 
_i_size = 0; 
_i_current = 0; 

  goto L_look__start;

}/* atomic */

L_look__start : 
atomic{
   /* EXE (line: 634)  */ 
_i_current = 0; 

  goto L_look__rl;

}/* atomic */

L_look__rl : 
atomic{
do
  :: (C_buffer2queue_ch?[S_init_terminal,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_init_terminal  (aSENDER, SENDER_PID, _, _, _, _); 

  goto L_look__read_1;

  :: (C_buffer2queue_ch?[S_ready,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_ready  (aSENDER, SENDER_PID, _, _, _, _); 

  goto L_look__read_2;

  :: (C_buffer2queue_ch?[S_init_cl,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_init_cl  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_cl_operation, SENDER_PID); 

  goto L_look__read_3;

  :: (C_buffer2queue_ch?[S_kill_cl,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_kill_cl  (aSENDER, SENDER_PID, _, _, _, _); 

  goto L_look__read_4;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_init_terminal   )  -> 
    /* EXE (line: 683)  */ 
terminal_count = terminal_count+1; 
pid_to_idx[_sender_list[_i_current]] = terminal_count; 
terminal_inst[terminal_count] = _sender_list[_i_current]; 
terminal_num = pid_to_idx[_sender_list[_i_current]]; 
   /* call inline */ shift_arrays(_i_size, _i_current); /* line: 687 */ 

  goto L_look__start;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_ready   )  -> 
    /* EXE (line: 693)  */ 
terminal_num = pid_to_idx[_sender_list[_i_current]]; 
   /* call inline */ shift_arrays(_i_size, _i_current); /* line: 694 */ 

  goto L_next_client__start;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_init_cl   )  -> 
    /* EXE (line: 700)  */ 
_i_current = _i_current+1; 

  goto L_look__rl;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_kill_cl   )  -> 
    /* EXE (line: 706)  */ 
count = count-1; 
   /* call inline */ shift_arrays(_i_size, _i_current); /* line: 707 */ 

  goto L_look__start;

od;
}/* atomic */

L_look__read_1 : 
atomic{
   /* EXE (line: 644)  */ 
_signal_list[_i_size] = m_init_terminal; 
   /* call inline */ add_signal(0, 0, 0, 0); /* line: 645 */ 

  goto L_look__rl;

}/* atomic */

L_look__read_2 : 
atomic{
   /* EXE (line: 654)  */ 
_signal_list[_i_size] = m_ready; 
   /* call inline */ add_signal(0, 0, 0, 0); /* line: 655 */ 

  goto L_look__rl;

}/* atomic */

L_look__read_3 : 
atomic{
   /* EXE (line: 665)  */ 
_signal_list[_i_size] = m_init_cl; 
   /* call inline */ add_signal(_temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_cl_operation); /* line: 666 */ 

  goto L_look__rl;

}/* atomic */

L_look__read_4 : 
atomic{
   /* EXE (line: 676)  */ 
_signal_list[_i_size] = m_kill_cl; 
   /* call inline */ add_signal(0, 0, 0, 0); /* line: 677 */ 

  goto L_look__rl;

}/* atomic */

L_next_client__start : 
atomic{
   /* EXE (line: 712)  */ 
_i_current = 0; 

  goto L_next_client__rl;

}/* atomic */

L_next_client__rl : 
atomic{
do
  :: (C_buffer2queue_ch?[S_init_terminal,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_init_terminal  (aSENDER, SENDER_PID, _, _, _, _); 

  goto L_next_client__read_1;

  :: (C_buffer2queue_ch?[S_ready,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_ready  (aSENDER, SENDER_PID, _, _, _, _); 

  goto L_next_client__read_2;

  :: (C_buffer2queue_ch?[S_init_cl,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_init_cl  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_cl_operation, SENDER_PID); 

  goto L_next_client__read_3;

  :: (C_buffer2queue_ch?[S_kill_cl,_,_,_,_,_,_]) -> 
     C_buffer2queue_ch ? S_kill_cl  (aSENDER, SENDER_PID, _, _, _, _); 

  goto L_next_client__read_4;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_init_terminal   )  -> 
    /* EXE (line: 761)  */ 
_i_current = _i_current+1; 

  goto L_next_client__rl;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_ready   )  -> 
    /* EXE (line: 767)  */ 
_i_current = _i_current+1; 

  goto L_next_client__rl;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_init_cl   )  -> 
    /* EXE (line: 773)  */ 
cl_summ = _sig_p1[_i_current]; 
cl_card_num = _sig_p2[_i_current]; 
cl_code = _sig_p3[_i_current]; 
cl_operation = _sig_p4[_i_current]; 
   /* call inline */ shift_arrays(_i_size, _i_current); /* line: 777 */ 

  goto L_next_client__X1;

  ::  (  _i_size!=0 AND  _i_current<_i_size AND _signal_list[_i_current]==m_kill_cl   )  -> 
    /* EXE (line: 798)  */ 
count = count-1; 
   /* call inline */ shift_arrays(_i_size, _i_current); /* line: 799 */ 

  goto L_next_client__start;

od;
}/* atomic */

L_next_client__read_1 : 
atomic{
   /* EXE (line: 722)  */ 
_signal_list[_i_size] = m_init_terminal; 
   /* call inline */ add_signal(0, 0, 0, 0); /* line: 723 */ 

  goto L_next_client__rl;

}/* atomic */

L_next_client__read_2 : 
atomic{
   /* EXE (line: 732)  */ 
_signal_list[_i_size] = m_ready; 
   /* call inline */ add_signal(0, 0, 0, 0); /* line: 733 */ 

  goto L_next_client__rl;

}/* atomic */

L_next_client__read_3 : 
atomic{
   /* EXE (line: 743)  */ 
_signal_list[_i_size] = m_init_cl; 
   /* call inline */ add_signal(_temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_cl_operation); /* line: 744 */ 

  goto L_next_client__rl;

}/* atomic */

L_next_client__read_4 : 
atomic{
   /* EXE (line: 754)  */ 
_signal_list[_i_size] = m_kill_cl; 
   /* call inline */ add_signal(0, 0, 0, 0); /* line: 755 */ 

  goto L_next_client__rl;

}/* atomic */

L_next_client__X1 : 
atomic{
   /* CREATE (line: 782)  */ 
 /* aOFFSPRING = run client(); */  
  /* ++ CREATE PROCESS client ++ */
  atomic {
      aOFFSPRING = run client(); 
      /* reset the PROC_INIT[] item, since pids can be reused ! */
      PROC_INIT[ aOFFSPRING ] = 0;
      PARENT[ aOFFSPRING ] = _pid; 
  
      PROC_TYPE[ aOFFSPRING ] = proc_client;
    /*   CUR_PROC_COUNT[ proc_client ]++; */
  }
  /* wait for 'client' to update its CV_<channels>[] and set PROC_INIT[] to 1 to flag it is ready */  
  PROC_INIT[ aOFFSPRING ] > 0;
  /* ^^ CREATE PROCESS client ^^ */

  goto L_next_client__X2;

}/* atomic */

L_next_client__X2 : 
atomic{
    printf("WRITE: C_queue2client_ch ! S_init( _pid cl_summ cl_card_num cl_code terminal_inst[terminal_num] cl_operation aOFFSPRING ) ;\n");
  C_queue2client_ch ! S_init( _pid, cl_summ, cl_card_num, cl_code, terminal_inst[terminal_num], cl_operation, aOFFSPRING ) ;

  goto L_next_client__X3;

}/* atomic */

L_next_client__X3 : 
atomic{
   /* EXE (line: 792)  */ 
count = count+1; 

  goto L_look__start;

}/* atomic */

L___end_of_process__: printf("[END OF PROCESS queue(%d) WITH PID %d]\n", proc_queue, _pid);
              CUR_PROC_COUNT[ proc_queue ]--;
              PROC_TYPE[ _pid ] = 0;
} /* END PROCESS queue */ 

/* -- globals_for_instances -- already made */
/* Processing PROCESS server */
/* ----------------------                 BEGIN PROCESS 'server'   ---------------- */
active proctype server() { /* BEGIN PROCESS server */ 
  /* process_local_vars_channels (..., ) */
  /* ++ locals_for_instances ++ */
  /* Needed for creation only */
  local byte aOFFSPRING; 
  local byte aSENDER; 
  local byte aPARENT; 
  
  
  CUR_PROC_COUNT[ proc_server ]++;
  
  
  /* Needed for time intervals */
  local int startTick; 
  /* ^^ locals_for_instances ^^ */
  /* process_local_vars_channels (..., AFTER VARS) */
/* SENDER_PID OF PId */ 
PId SENDER_PID; 
/* cl_summ OF integer */ 
int cl_summ; 
/* cl_card_num OF integer */ 
int cl_card_num; 
/* cl_code OF integer */ 
int cl_code; 
L_Start__rl : 
atomic{
   /* EXE (line: 813)  */ 
code_table[1] = 12; 
code_table[2] = 23; 
code_table[3] = 34; 
summ_table[1] = 10; 
summ_table[2] = 40; 
summ_table[3] = 80; 

  goto L_main__rl;

}/* atomic */

L_main__rl : 
atomic{
do
  :: (C_buffer2server_ch?[S_check_code,_,_,_,_]) -> 
     C_buffer2server_ch ? S_check_code  (aSENDER, cl_card_num, cl_code, SENDER_PID); 

  goto L_begin_decision__1;

  :: (C_buffer2server_ch?[S_check_summ,_,_,_,_]) -> 
     C_buffer2server_ch ? S_check_summ  (aSENDER, cl_card_num, cl_summ, SENDER_PID); 

  goto L_begin_decision__2;

  :: (C_buffer2server_ch?[S_balance,_,_,_,_]) -> 
     C_buffer2server_ch ? S_balance  (aSENDER, cl_card_num, SENDER_PID, _); 

  goto L_main__X6;

  :: (C_buffer2server_ch?[S_insert_summ,_,_,_,_]) -> 
     C_buffer2server_ch ? S_insert_summ  (aSENDER, cl_card_num, cl_summ, SENDER_PID); 

  goto L_main__X7;

od;
}/* atomic */

L_begin_decision__1 : 
atomic{
   /* EXE (line: 828)  */ 

  goto L_main__1;

}/* atomic */

L_main__1 : 
atomic{
do
  ::  ( code_table[cl_card_num]!=cl_code )  -> 
    /* EXE (line: 834)  */ 

  goto L_main__1__X1;

  ::  ( !( code_table[cl_card_num]!=cl_code ) )  -> 
    /* EXE (line: 845)  */ 

  goto L_main__1__X2;

od;
}/* atomic */

L_main__1__X1 : 
atomic{
    printf("WRITE: C_server2terminal_ch ! S_wrong_code( _pid SENDER_PID 99 ) ;\n");
  C_server2terminal_ch ! S_wrong_code( _pid, SENDER_PID, 99 ) ;

  goto L_main__rl;

}/* atomic */

L_main__1__X2 : 
atomic{
    printf("WRITE: C_server2terminal_ch ! S_correct_code( _pid SENDER_PID 99 ) ;\n");
  C_server2terminal_ch ! S_correct_code( _pid, SENDER_PID, 99 ) ;

  goto L_main__rl;

}/* atomic */

L_end_decision__1 : 
atomic{
   /* EXE (line: 855)  */ 

  goto L_end_of_process;

}/* atomic */

L_begin_decision__2 : 
atomic{
   /* EXE (line: 865)  */ 

  goto L_main__2;

}/* atomic */

L_main__2 : 
atomic{
do
  ::  ( summ_table[cl_card_num]<=cl_summ )  -> 
    /* EXE (line: 871)  */ 

  goto L_main__2__X3;

  ::  ( !( summ_table[cl_card_num]<=cl_summ ) )  -> 
    /* EXE (line: 882)  */ 

  goto L_main__2__X4;

od;
}/* atomic */

L_main__2__X3 : 
atomic{
    printf("WRITE: C_server2terminal_ch ! S_no_money( _pid SENDER_PID 99 ) ;\n");
  C_server2terminal_ch ! S_no_money( _pid, SENDER_PID, 99 ) ;

  goto L_main__rl;

}/* atomic */

L_main__2__X4 : 
atomic{
    printf("WRITE: C_server2terminal_ch ! S_summ( _pid cl_summ SENDER_PID ) ;\n");
  C_server2terminal_ch ! S_summ( _pid, cl_summ, SENDER_PID ) ;

  goto L_main__2__X5;

}/* atomic */

L_main__2__X5 : 
atomic{
   /* EXE (line: 892)  */ 
summ_table[cl_card_num] = summ_table[cl_card_num]-cl_summ; 

  goto L_main__rl;

}/* atomic */

L_end_decision__2 : 
atomic{
   /* EXE (line: 897)  */ 

  goto L_end_of_process;

}/* atomic */

L_main__X6 : 
atomic{
    printf("WRITE: C_server2terminal_ch ! S_summ( _pid summ_table[cl_card_num] SENDER_PID ) ;\n");
  C_server2terminal_ch ! S_summ( _pid, summ_table[cl_card_num], SENDER_PID ) ;

  goto L_main__rl;

}/* atomic */

L_main__X7 : 
atomic{
   /* EXE (line: 917)  */ 
summ_table[cl_card_num] = summ_table[cl_card_num]+cl_summ; 

  goto L_main__X8;

}/* atomic */

L_main__X8 : 
atomic{
    printf("WRITE: C_server2terminal_ch ! S_summ( _pid summ_table[cl_card_num] SENDER_PID ) ;\n");
  C_server2terminal_ch ! S_summ( _pid, summ_table[cl_card_num], SENDER_PID ) ;

  goto L_main__rl;

}/* atomic */

 /* Fix for non-existing state */
 L_end_of_process: goto L___end_of_process__;
L___end_of_process__: printf("[END OF PROCESS server(%d) WITH PID %d]\n", proc_server, _pid);
              CUR_PROC_COUNT[ proc_server ]--;
              PROC_TYPE[ _pid ] = 0;
} /* END PROCESS server */ 

/* -- globals_for_instances -- already made */
/* Processing PROCESS client */
/* ----------------------                 BEGIN PROCESS 'client'   ---------------- */
proctype client() { /* BEGIN PROCESS client (0, 250) */ 
  /* process_local_vars_channels (..., ) */
  /* ++ locals_for_channel_instances ++ () */
  chan CVL_buffer2client_ch = [10] of { mtype, byte, int, PId, int, PId, int, PId } ; 
  /* ++ locals_for_instances ++ */
  /* Needed for creation only */
  local byte aOFFSPRING; 
  local byte aSENDER; 
  local byte aPARENT; 
  
  
  CUR_PROC_COUNT[ proc_client ]++;
  
  
  /* Needed for time intervals */
  local int startTick; 
  /* ^^ locals_for_instances ^^ */
  /* process_local_vars_channels (..., AFTER VARS) */
  /* ++ locals_for_channel_instances ++ (AFTER VARS) */
  atomic{
    /*  PROC_INIT[ _pid ] = 0; */
      CV_buffer2client_ch[ _pid ] = CVL_buffer2client_ch ; 
      PROC_INIT[ _pid ] = 1;
      aPARENT = PARENT[ _pid ]; 
  }
  /* ^^ locals_for_channel_instances ^^ (AFTER VARS) */
/* SENDER_PID OF PId */ 
PId SENDER_PID; 
/* terminal_pid OF PId */ 
PId terminal_pid; 
/* cl_summ OF integer */ 
int cl_summ; 
/* cl_card_num OF integer */ 
int cl_card_num; 
/* cl_code OF integer */ 
int cl_code; 
/* g_summ OF integer */ 
int g_summ; 
/* is_terminal_ready OF integer */ 
int is_terminal_ready; 
/* cl_operation OF integer */ 
int cl_operation; 
/* _temp_terminal_pid OF PId */ 
PId _temp_terminal_pid; 
/* _temp_cl_summ OF integer */ 
int _temp_cl_summ; 
/* _temp_cl_card_num OF integer */ 
int _temp_cl_card_num; 
/* _temp_cl_code OF integer */ 
int _temp_cl_code; 
/* _temp_g_summ OF integer */ 
int _temp_g_summ; 
/* _temp_cl_operation OF integer */ 
int _temp_cl_operation; 
L_Start__rl : 
atomic{
   /* EXE (line: 945)  */ 

  goto L_init_client__rl;

}/* atomic */

L_init_client__rl : 
atomic{
do
  :: (CVL_buffer2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_init  (aSENDER, cl_summ, cl_card_num, cl_code, terminal_pid, cl_operation, SENDER_PID); 

  goto L_init_client__X1;

  :: (CVL_buffer2client_ch?[S_ready_for_job,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_ready_for_job  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_correct_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_wrong_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_wrong_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_correct_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_money  (aSENDER, _temp_g_summ, SENDER_PID, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_cheque,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_cheque  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_limit,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_limit  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_terminal_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_terminal_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_init_client__rl;

  :: (CVL_buffer2client_ch?[S_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID, _, _, _, _); 

  goto L_init_client__rl;

od;
}/* atomic */

L_init_client__X1 : 
atomic{
   /* EXE (line: 955)  */ 
wants_summ[SELF] = cl_summ; 

  goto L_init_client__X2;

}/* atomic */

L_init_client__X2 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_press_start( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_press_start( _pid, terminal_pid, 99 ) ;

  goto L_wait_terminal__rl;

}/* atomic */

L_wait_terminal__rl : 
atomic{
do
  :: (CVL_buffer2client_ch?[S_ready_for_job,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_ready_for_job  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__X1;

  :: (CVL_buffer2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_init  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_terminal_pid, _temp_cl_operation, SENDER_PID); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_correct_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_wrong_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_wrong_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_correct_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_money  (aSENDER, _temp_g_summ, SENDER_PID, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_cheque,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_cheque  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_limit,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_limit  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_terminal_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_terminal_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_wait_terminal__rl;

  :: (CVL_buffer2client_ch?[S_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID, _, _, _, _); 

  goto L_wait_terminal__rl;

od;
}/* atomic */

L_wait_terminal__X1 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_card( _pid cl_card_num terminal_pid ) ;\n");
  C_client2terminal_ch ! S_card( _pid, cl_card_num, terminal_pid ) ;

  goto L_look__rl;

}/* atomic */

L_look__rl : 
atomic{
do
  :: (CVL_buffer2client_ch?[S_correct_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__X1;

  :: (CVL_buffer2client_ch?[S_wrong_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__X2;

  :: (CVL_buffer2client_ch?[S_correct_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_begin_decision__1;

  :: (CVL_buffer2client_ch?[S_wrong_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__X5;

  :: (CVL_buffer2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_init  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_terminal_pid, _temp_cl_operation, SENDER_PID); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_ready_for_job,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_ready_for_job  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_money  (aSENDER, _temp_g_summ, SENDER_PID, _, _, _, _); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_cheque,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_cheque  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_limit,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_limit  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_terminal_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_terminal_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_look__rl;

  :: (CVL_buffer2client_ch?[S_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID, _, _, _, _); 

  goto L_look__rl;

od;
}/* atomic */

L_look__X1 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_code( _pid cl_code terminal_pid ) ;\n");
  C_client2terminal_ch ! S_code( _pid, cl_code, terminal_pid ) ;

  goto L_look__rl;

}/* atomic */

L_look__X2 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_done( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_done( _pid, terminal_pid, 99 ) ;

  goto L_look__X3;

}/* atomic */

L_look__X3 : 
atomic{
    printf("WRITE: C_client2queue_ch ! S_kill_cl( _pid Null ) ;\n");
  C_client2queue_ch ! S_kill_cl( _pid, Null ) ;

  goto L_no_card__rl;

}/* atomic */

L_no_card__rl : 
atomic{
   /* EXE (line: 1113)  */ 

  goto L_no_card__stop__rl;

}/* atomic */

L_begin_decision__1 : 
atomic{
   /* EXE (line: 1123)  */ 

  goto L_look__1;

}/* atomic */

L_look__1 : 
atomic{
do
  ::  ( cl_operation==1 )  -> 
    /* EXE (line: 1129)  */ 

  goto L_look__1__X4;

  ::  ( cl_operation==2 )  -> 
    /* EXE (line: 1140)  */ 

  goto L_look__1__X5;

  ::  ( cl_operation==3 )  -> 
    /* EXE (line: 1156)  */ 

  goto L_look__1__X7;

od;
}/* atomic */

L_look__1__X4 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_get_balance( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_get_balance( _pid, terminal_pid, 99 ) ;

  goto L_get_cheque__rl;

}/* atomic */

L_look__1__X5 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_get_money( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_get_money( _pid, terminal_pid, 99 ) ;

  goto L_look__1__X6;

}/* atomic */

L_look__1__X6 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_summ( _pid cl_summ terminal_pid ) ;\n");
  C_client2terminal_ch ! S_summ( _pid, cl_summ, terminal_pid ) ;

  goto L_get_money__rl;

}/* atomic */

L_look__1__X7 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_insert_money( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_insert_money( _pid, terminal_pid, 99 ) ;

  goto L_look__1__X8;

}/* atomic */

L_look__1__X8 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_summ( _pid cl_summ terminal_pid ) ;\n");
  C_client2terminal_ch ! S_summ( _pid, cl_summ, terminal_pid ) ;

  goto L_get_cheque__rl;

}/* atomic */

L_get_money__rl : 
atomic{
do
  :: (CVL_buffer2client_ch?[S_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_money  (aSENDER, g_summ, SENDER_PID, _, _, _, _); 

  goto L_get_money__X1;

  :: (CVL_buffer2client_ch?[S_limit,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_limit  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__X2;

  :: (CVL_buffer2client_ch?[S_terminal_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_terminal_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__X4;

  :: (CVL_buffer2client_ch?[S_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__X6;

  :: (CVL_buffer2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_init  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_terminal_pid, _temp_cl_operation, SENDER_PID); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_ready_for_job,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_ready_for_job  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_correct_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_wrong_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_wrong_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_correct_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_cheque,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_cheque  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_money__rl;

  :: (CVL_buffer2client_ch?[S_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID, _, _, _, _); 

  goto L_get_money__rl;

od;
}/* atomic */

L_look__X5 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_done( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_done( _pid, terminal_pid, 99 ) ;

  goto L_look__X6;

}/* atomic */

L_look__X6 : 
atomic{
    printf("WRITE: C_client2queue_ch ! S_kill_cl( _pid Null ) ;\n");
  C_client2queue_ch ! S_kill_cl( _pid, Null ) ;

  goto L_no_code__rl;

}/* atomic */

L_no_code__rl : 
atomic{
   /* EXE (line: 1191)  */ 

  goto L_no_code__stop__rl;

}/* atomic */

L_get_money__X1 : 
atomic{
   /* EXE (line: 1238)  */ 
gotten_summ[SELF] = g_summ; 

  goto L_get_cheque__rl;

}/* atomic */

L_get_cheque__rl : 
atomic{
do
  :: (CVL_buffer2client_ch?[S_cheque,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_cheque  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_init  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_terminal_pid, _temp_cl_operation, SENDER_PID); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_ready_for_job,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_ready_for_job  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_correct_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_wrong_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_wrong_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_correct_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_money  (aSENDER, _temp_g_summ, SENDER_PID, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_limit,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_limit  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_terminal_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_terminal_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_cheque__rl;

  :: (CVL_buffer2client_ch?[S_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID, _, _, _, _); 

  goto L_get_cheque__rl;

od;
}/* atomic */

L_get_money__X2 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_done( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_done( _pid, terminal_pid, 99 ) ;

  goto L_get_money__X3;

}/* atomic */

L_get_money__X3 : 
atomic{
    printf("WRITE: C_client2queue_ch ! S_kill_cl( _pid Null ) ;\n");
  C_client2queue_ch ! S_kill_cl( _pid, Null ) ;

  goto L_limit_money__rl;

}/* atomic */

L_limit_money__rl : 
atomic{
   /* EXE (line: 1263)  */ 

  goto L_limit_money__stop__rl;

}/* atomic */

L_get_money__X4 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_done( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_done( _pid, terminal_pid, 99 ) ;

  goto L_get_money__X5;

}/* atomic */

L_get_money__X5 : 
atomic{
    printf("WRITE: C_client2queue_ch ! S_kill_cl( _pid Null ) ;\n");
  C_client2queue_ch ! S_kill_cl( _pid, Null ) ;

  goto L_term_no_money__rl;

}/* atomic */

L_term_no_money__rl : 
atomic{
   /* EXE (line: 1283)  */ 

  goto L_term_no_money__stop__rl;

}/* atomic */

L_get_money__X6 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_done( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_done( _pid, terminal_pid, 99 ) ;

  goto L_get_money__X7;

}/* atomic */

L_get_money__X7 : 
atomic{
    printf("WRITE: C_client2queue_ch ! S_kill_cl( _pid Null ) ;\n");
  C_client2queue_ch ! S_kill_cl( _pid, Null ) ;

  goto L_cl_no_money__rl;

}/* atomic */

L_cl_no_money__rl : 
atomic{
   /* EXE (line: 1303)  */ 

  goto L_cl_no_money__stop__rl;

}/* atomic */

L_get_card__rl : 
atomic{
do
  :: (CVL_buffer2client_ch?[S_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_card  (aSENDER, cl_card_num, SENDER_PID, _, _, _, _); 

  goto L_get_card__X1;

  :: (CVL_buffer2client_ch?[S_init,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_init  (aSENDER, _temp_cl_summ, _temp_cl_card_num, _temp_cl_code, _temp_terminal_pid, _temp_cl_operation, SENDER_PID); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_ready_for_job,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_ready_for_job  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_correct_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_wrong_card,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_card  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_wrong_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_wrong_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_correct_code,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_correct_code  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_money  (aSENDER, _temp_g_summ, SENDER_PID, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_cheque,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_cheque  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_limit,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_limit  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_terminal_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_terminal_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

  :: (CVL_buffer2client_ch?[S_no_money,_,_,_,_,_,_,_]) -> 
     CVL_buffer2client_ch ? S_no_money  (aSENDER, SENDER_PID, _, _, _, _, _); 

  goto L_get_card__rl;

od;
}/* atomic */

L_get_card__X1 : 
atomic{
    printf("WRITE: C_client2terminal_ch ! S_done( _pid terminal_pid 99 ) ;\n");
  C_client2terminal_ch ! S_done( _pid, terminal_pid, 99 ) ;

  goto L_get_card__X2;

}/* atomic */

L_get_card__X2 : 
atomic{
    printf("WRITE: C_client2queue_ch ! S_kill_cl( _pid Null ) ;\n");
  C_client2queue_ch ! S_kill_cl( _pid, Null ) ;

  goto L_good__rl;

}/* atomic */

L_good__rl : 
atomic{
   /* EXE (line: 1477)  */ 

  goto L_good__stop__rl;

}/* atomic */

L_no_code__stop__rl : 
atomic{
   /* STOP (line: 1483)  */ 
 goto L___end_of_process__; 

  goto L_end_of_process;

}/* atomic */

L_no_card__stop__rl : 
atomic{
   /* STOP (line: 1489)  */ 
 goto L___end_of_process__; 

  goto L_end_of_process;

}/* atomic */

L_good__stop__rl : 
atomic{
   /* STOP (line: 1495)  */ 
 goto L___end_of_process__; 

  goto L_end_of_process;

}/* atomic */

L_cl_no_money__stop__rl : 
atomic{
   /* STOP (line: 1501)  */ 
 goto L___end_of_process__; 

  goto L_end_of_process;

}/* atomic */

L_limit_money__stop__rl : 
atomic{
   /* STOP (line: 1507)  */ 
 goto L___end_of_process__; 

  goto L_end_of_process;

}/* atomic */

L_term_no_money__stop__rl : 
atomic{
   /* STOP (line: 1513)  */ 
 goto L___end_of_process__; 

  goto L_end_of_process;

}/* atomic */

 /* Fix for non-existing state */
 L_end_of_process: goto L___end_of_process__;
L___end_of_process__: printf("[END OF PROCESS client(%d) WITH PID %d]\n", proc_client, _pid);
              CUR_PROC_COUNT[ proc_client ]--;
              PROC_TYPE[ _pid ] = 0;
} /* END PROCESS client */ 

/* -- globals_for_instances -- already made */
/* Processing PROCESS terminal */
/* ----------------------                 BEGIN PROCESS 'terminal'   ---------------- */
active [6] proctype terminal() { /* BEGIN PROCESS terminal (6, 6) */ 
  /* process_local_vars_channels (..., ) */
  /* ++ locals_for_channel_instances ++ () */
  chan CVL_buffer2terminal_ch = [10] of { mtype, byte, int, PId } ; 
  /* ++ locals_for_instances ++ */
  /* Needed for creation only */
  local byte aOFFSPRING; 
  local byte aSENDER; 
  local byte aPARENT; 
  
  
  CUR_PROC_COUNT[ proc_terminal ]++;
  
  
  /* Needed for time intervals */
  local int startTick; 
  /* ^^ locals_for_instances ^^ */
  /* process_local_vars_channels (..., AFTER VARS) */
  /* ++ locals_for_channel_instances ++ (AFTER VARS) */
  atomic{
    /*  PROC_INIT[ _pid ] = 0; */
      CV_buffer2terminal_ch[ _pid ] = CVL_buffer2terminal_ch ; 
      PROC_INIT[ _pid ] = 1;
      aPARENT = PARENT[ _pid ]; 
  }
  /* ^^ locals_for_channel_instances ^^ (AFTER VARS) */
/* SENDER_PID OF PId */ 
PId SENDER_PID; 
/* client_pid OF PId */ 
PId client_pid; 
/* one_time_limit OF integer */ 
int one_time_limit; 
/* terminal_summ OF integer */ 
int terminal_summ; 
/* cl_summ OF integer */ 
int cl_summ; 
/* cl_card_num OF integer */ 
int cl_card_num; 
/* cl_code OF integer */ 
int cl_code; 
/* output_summ OF integer */ 
int output_summ; 
/* _temp_cl_summ OF integer */ 
int _temp_cl_summ; 
/* _temp_cl_card_num OF integer */ 
int _temp_cl_card_num; 
/* _temp_cl_code OF integer */ 
int _temp_cl_code; 
/* _temp_output_summ OF integer */ 
int _temp_output_summ; 
L_Start__rl : 
atomic{
   /* EXE (line: 1534)  */ 
terminal_summ = 100; 
one_time_limit = 90; 

  goto L_Start__X1;

}/* atomic */

L_Start__X1 : 
atomic{
    printf("WRITE: C_terminal2queue_ch ! S_init_terminal( _pid Null ) ;\n");
  C_terminal2queue_ch ! S_init_terminal( _pid, Null ) ;

  goto L_Start__X2;

}/* atomic */

L_Start__X2 : 
atomic{
    printf("WRITE: C_terminal2queue_ch ! S_ready( _pid Null ) ;\n");
  C_terminal2queue_ch ! S_ready( _pid, Null ) ;

  goto L_main__rl;

}/* atomic */

L_main__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_main__X1;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, _temp_cl_summ, SENDER_PID); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_main__rl;

od;
}/* atomic */

L_main__X1 : 
atomic{
   /* EXE (line: 1555)  */ 
cl_summ = 0; 
cl_card_num = 0; 
cl_code = 0; 

  goto L_main__X2;

}/* atomic */

L_main__X2 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_ready_for_job( _pid SENDER_PID 99 ) ;\n");
  C_terminal2client_ch ! S_ready_for_job( _pid, SENDER_PID, 99 ) ;

  goto L_wait_for_card__rl;

}/* atomic */

L_wait_for_card__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, cl_card_num, SENDER_PID); 

  goto L_wait_for_card__X1;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, _temp_cl_summ, SENDER_PID); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_card__rl;

od;
}/* atomic */

L_wait_for_card__X1 : 
atomic{
   /* EXE (line: 1622)  */ 
client_pid = SENDER_PID; 

  goto L_wait_for_card__X2;

}/* atomic */

L_wait_for_card__X2 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_correct_card( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_correct_card( _pid, client_pid, 99 ) ;

  goto L_wait_for_code__rl;

}/* atomic */

L_wait_for_code__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, cl_code, SENDER_PID); 

  goto L_wait_for_code__X1;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, _temp_cl_summ, SENDER_PID); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_code__rl;

od;
}/* atomic */

L_wait_for_code__X1 : 
atomic{
    printf("WRITE: C_terminal2server_ch ! S_check_code( _pid cl_card_num cl_code Null ) ;\n");
  C_terminal2server_ch ! S_check_code( _pid, cl_card_num, cl_code, Null ) ;

  goto L_check_code__rl;

}/* atomic */

L_check_code__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_check_code__X1;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_check_code__X2;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, _temp_cl_summ, SENDER_PID); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_check_code__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_check_code__rl;

od;
}/* atomic */

L_check_code__X1 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_correct_code( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_correct_code( _pid, client_pid, 99 ) ;

  goto L_wait_for_operation__rl;

}/* atomic */

L_wait_for_operation__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_operation__X1;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, _temp_cl_summ, SENDER_PID); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_operation__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_operation__rl;

od;
}/* atomic */

L_wait_for_operation__X1 : 
atomic{
    printf("WRITE: C_terminal2server_ch ! S_balance( _pid cl_card_num Null 99 ) ;\n");
  C_terminal2server_ch ! S_balance( _pid, cl_card_num, Null, 99 ) ;

  goto L_wait_for_balance__rl;

}/* atomic */

L_wait_for_balance__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, output_summ, SENDER_PID); 

  goto L_wait_for_balance__X1;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_balance__rl;

od;
}/* atomic */

L_wait_for_balance__X1 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_cheque( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_cheque( _pid, client_pid, 99 ) ;

  goto L_wait_for_balance__X2;

}/* atomic */

L_wait_for_balance__X2 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_card( _pid cl_card_num client_pid ) ;\n");
  C_terminal2client_ch ! S_card( _pid, cl_card_num, client_pid ) ;

  goto L_satisfaction__rl;

}/* atomic */

L_wait_for_insert__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, cl_summ, SENDER_PID); 

  goto L_wait_for_insert__X1;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_insert__rl;

od;
}/* atomic */

L_wait_for_insert__X1 : 
atomic{
    printf("WRITE: C_terminal2server_ch ! S_insert_summ( _pid cl_card_num cl_summ Null ) ;\n");
  C_terminal2server_ch ! S_insert_summ( _pid, cl_card_num, cl_summ, Null ) ;

  goto L_wait_for_balance__rl;

}/* atomic */

L_wait_for_summ__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, cl_summ, SENDER_PID); 

  goto L_begin_decision__1;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_wait_for_summ__rl;

od;
}/* atomic */

L_check_code__X2 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_wrong_code( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_wrong_code( _pid, client_pid, 99 ) ;

  goto L_satisfaction__rl;

}/* atomic */

L_satisfaction__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__X1;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, _temp_cl_summ, SENDER_PID); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_satisfaction__rl;

od;
}/* atomic */

L_begin_decision__1 : 
atomic{
   /* EXE (line: 2065)  */ 

  goto L_wait_for_summ__1;

}/* atomic */

L_wait_for_summ__1 : 
atomic{
do
  ::  ( cl_summ>terminal_summ )  -> 
    /* EXE (line: 2071)  */ 

  goto L_wait_for_summ__1__X1;

  ::  ( cl_summ>one_time_limit )  -> 
    /* EXE (line: 2082)  */ 

  goto L_wait_for_summ__1__X2;

  ::  ( !( ( cl_summ>terminal_summ )OR ( cl_summ>one_time_limit )  ) )  -> 
    /* EXE (line: 2093)  */ 

  goto L_wait_for_summ__1__X3;

od;
}/* atomic */

L_wait_for_summ__1__X1 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_terminal_no_money( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_terminal_no_money( _pid, client_pid, 99 ) ;

  goto L_satisfaction__rl;

}/* atomic */

L_wait_for_summ__1__X2 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_limit( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_limit( _pid, client_pid, 99 ) ;

  goto L_wait_for_summ__1;

}/* atomic */

L_wait_for_summ__1__X3 : 
atomic{
    printf("WRITE: C_terminal2server_ch ! S_check_summ( _pid cl_card_num cl_summ Null ) ;\n");
  C_terminal2server_ch ! S_check_summ( _pid, cl_card_num, cl_summ, Null ) ;

  goto L_check_summ__rl;

}/* atomic */

L_check_summ__rl : 
atomic{
do
  :: (CVL_buffer2terminal_ch?[S_no_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_no_money  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__X1;

  :: (CVL_buffer2terminal_ch?[S_summ,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_summ  (aSENDER, output_summ, SENDER_PID); 

  goto L_check_summ__X2;

  :: (CVL_buffer2terminal_ch?[S_press_start,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_press_start  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_card,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_card  (aSENDER, _temp_cl_card_num, SENDER_PID); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_code  (aSENDER, _temp_cl_code, SENDER_PID); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_correct_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_correct_code  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_wrong_code,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_wrong_code  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_done,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_done  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_get_balance,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_balance  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_get_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_get_money  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

  :: (CVL_buffer2terminal_ch?[S_insert_money,_,_,_]) -> 
     CVL_buffer2terminal_ch ? S_insert_money  (aSENDER, SENDER_PID, _); 

  goto L_check_summ__rl;

od;
}/* atomic */

L_end_decision__1 : 
atomic{
   /* EXE (line: 2109)  */ 

  goto L_end_of_process;

}/* atomic */

L_check_summ__X1 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_no_money( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_no_money( _pid, client_pid, 99 ) ;

  goto L_satisfaction__rl;

}/* atomic */

L_check_summ__X2 : 
atomic{
   /* EXE (line: 2176)  */ 
terminal_summ = terminal_summ-output_summ; 

  goto L_check_summ__X3;

}/* atomic */

L_check_summ__X3 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_money( _pid output_summ client_pid ) ;\n");
  C_terminal2client_ch ! S_money( _pid, output_summ, client_pid ) ;

  goto L_check_summ__X4;

}/* atomic */

L_check_summ__X4 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_cheque( _pid client_pid 99 ) ;\n");
  C_terminal2client_ch ! S_cheque( _pid, client_pid, 99 ) ;

  goto L_check_summ__X5;

}/* atomic */

L_check_summ__X5 : 
atomic{
    printf("WRITE: C_terminal2client_ch ! S_card( _pid cl_card_num client_pid ) ;\n");
  C_terminal2client_ch ! S_card( _pid, cl_card_num, client_pid ) ;

  goto L_satisfaction__rl;

}/* atomic */

L_satisfaction__X1 : 
atomic{
    printf("WRITE: C_terminal2queue_ch ! S_ready( _pid Null ) ;\n");
  C_terminal2queue_ch ! S_ready( _pid, Null ) ;

  goto L_main__rl;

}/* atomic */

 /* Fix for non-existing state */
 L_end_of_process: goto L___end_of_process__;
L___end_of_process__: printf("[END OF PROCESS terminal(%d) WITH PID %d]\n", proc_terminal, _pid);
              CUR_PROC_COUNT[ proc_terminal ]--;
              PROC_TYPE[ _pid ] = 0;
} /* END PROCESS terminal */ 

 /* block::out end (name=Client_and_Terminals) */ 
/* ----- ENV ----- */
active proctype env_process() { /* Put it last */
    /* Read data from the <basename(file)>.env_inc file  */
#include "in.env_inc"
}
/* ----- ENV ----- */
