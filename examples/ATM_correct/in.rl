/* Sdl to Real Translater*/
/* Banking: BLOCK *//*14*/
 
 
 
 Client_and_Terminals: BLOCK/*15*/
  TYPE int_arr__revealed IS
   integer ARRAY OF integer.
  TYPE integer__revealed IS
   PId ARRAY OF integer.
  TYPE arr_int IS
   integer ARRAY OF integer.
  PR VAR code_table OF int_arr__revealed./*414*/
  PR VAR summ_table OF int_arr__revealed./*414*/
  PR VAR wants_summ OF integer__revealed./*227*/
  PR VAR gotten_summ OF integer__revealed./*227*/
  INN UNB QUEUE CHN terminal2queue_ch/*78*/
   
    FOR ready
    WITH PAR p1 OF PId;
    FOR init_terminal
    WITH PAR p1 OF PId.
  INN UNB QUEUE CHN server2terminal_ch/*75*/
   
    FOR summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR no_money
    WITH PAR p1 OF PId;
    FOR correct_code
    WITH PAR p1 OF PId;
    FOR wrong_code
    WITH PAR p1 OF PId.
  INN UNB QUEUE CHN buffer2server_ch/*72*/
   
    FOR check_code
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF PId;
    FOR check_summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF PId;
    FOR balance
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR insert_summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF PId.
  INN UNB QUEUE CHN terminal2server_ch/*72*/
   
    FOR check_code
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF PId;
    FOR check_summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF PId;
    FOR balance
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR insert_summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF PId.
  INN UNB QUEUE CHN buffer2terminal_ch/*57*/
   
    FOR no_money
    WITH PAR p1 OF PId;
    FOR correct_code
    WITH PAR p1 OF PId;
    FOR wrong_code
    WITH PAR p1 OF PId;
    FOR code
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR done
    WITH PAR p1 OF PId;
    FOR get_balance
    WITH PAR p1 OF PId;
    FOR get_money
    WITH PAR p1 OF PId;
    FOR insert_money
    WITH PAR p1 OF PId;
    FOR press_start
    WITH PAR p1 OF PId;
    FOR card
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId.
  INN UNB QUEUE CHN client2terminal_ch/*57*/
   
    FOR card
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR press_start
    WITH PAR p1 OF PId;
    FOR insert_money
    WITH PAR p1 OF PId;
    FOR get_money
    WITH PAR p1 OF PId;
    FOR get_balance
    WITH PAR p1 OF PId;
    FOR done
    WITH PAR p1 OF PId;
    FOR summ
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR code
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId.
  INN UNB QUEUE CHN terminal2client_ch/*54*/
   
    FOR card
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR terminal_no_money
    WITH PAR p1 OF PId;
    FOR no_money
    WITH PAR p1 OF PId;
    FOR correct_card
    WITH PAR p1 OF PId;
    FOR limit
    WITH PAR p1 OF PId;
    FOR correct_code
    WITH PAR p1 OF PId;
    FOR wrong_card
    WITH PAR p1 OF PId;
    FOR wrong_code
    WITH PAR p1 OF PId;
    FOR ready_for_job
    WITH PAR p1 OF PId;
    FOR cheque
    WITH PAR p1 OF PId;
    FOR money
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId.
  INN UNB QUEUE CHN client2queue_ch/*51*/
   FOR kill_cl
   WITH PAR p1 OF PId.
  INN UNB QUEUE CHN buffer2client_ch/*48*/
    FOR card
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId;
    FOR terminal_no_money
    WITH PAR p1 OF PId;
    FOR no_money
    WITH PAR p1 OF PId;
    FOR correct_card
    WITH PAR p1 OF PId;
    FOR limit
    WITH PAR p1 OF PId;
    FOR correct_code
    WITH PAR p1 OF PId;
    FOR wrong_card
    WITH PAR p1 OF PId;
    FOR wrong_code
    WITH PAR p1 OF PId;
    FOR ready_for_job
    WITH PAR p1 OF PId;
    FOR init
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF integer,
    WITH PAR p4 OF PId,
    WITH PAR p5 OF integer,
    WITH PAR p6 OF PId;
    FOR cheque
    WITH PAR p1 OF PId;
    FOR money
    WITH PAR p1 OF integer,
    WITH PAR p2 OF PId.
  INN UNB QUEUE CHN queue2client_ch/*48*/
    FOR init
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF integer,
    WITH PAR p4 OF PId,
    WITH PAR p5 OF integer,
    WITH PAR p6 OF PId.
  INN UNB QUEUE CHN buffer2queue_ch/*45*/
   
    FOR ready
    WITH PAR p1 OF PId;
    FOR init_terminal
    WITH PAR p1 OF PId;
    FOR init_cl
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF integer,
    WITH PAR p4 OF integer,
    WITH PAR p5 OF PId;
    FOR kill_cl
    WITH PAR p1 OF PId.
  INP UNB QUEUE CHN ENV2queue_ch/*45*/
    FOR init_cl
    WITH PAR p1 OF integer,
    WITH PAR p2 OF integer,
    WITH PAR p3 OF integer,
    WITH PAR p4 OF integer,
    WITH PAR p5 OF PId.
  
   FROM terminal CHN terminal2queue_ch  TO buffer./*78*/
   FROM server CHN server2terminal_ch  TO buffer./*75*/
   FROM buffer CHN buffer2server_ch  TO server./*72*/
   FROM terminal CHN terminal2server_ch  TO buffer./*72*/
   FROM buffer CHN buffer2terminal_ch  TO terminal./*57*/
   FROM client CHN client2terminal_ch  TO buffer./*57*/
   FROM terminal CHN terminal2client_ch  TO buffer./*54*/
   FROM client CHN client2queue_ch  TO buffer./*51*/
   FROM buffer CHN buffer2client_ch  TO client./*48*/
   FROM queue CHN queue2client_ch  TO buffer./*48*/
   FROM buffer CHN buffer2queue_ch  TO queue./*45*/
  FROM ENV CHN ENV2queue_ch  TO buffer./*45*/
  
  
  buffer: PROCESS
   PR VAR p4_integer OF integer.
   PR VAR p5_integer OF integer.
   PR VAR p6_integer OF integer.
   PR VAR p4_PId OF PId.
   PR VAR p3_integer OF integer.
   PR VAR p2_integer OF integer.
   PR VAR p1_integer OF integer.
   PR VAR TARGET_PID OF PId.
   
   TRANSITION read_signal
    READ ready(TARGET_PID) FROM terminal2queue_ch
    FROM NOW TO INF
   JUMP write_ready_queue_ch.
   
   TRANSITION write_ready_queue_ch
    WRITE ready(SENDER) INTO buffer2queue_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ init_terminal(TARGET_PID) FROM terminal2queue_ch
    FROM NOW TO INF
   JUMP write_init_terminal_queue_ch.
   
   TRANSITION write_init_terminal_queue_ch
    WRITE init_terminal(SENDER) INTO buffer2queue_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ summ(p1_integer,TARGET_PID) FROM server2terminal_ch
    FROM NOW TO INF
   JUMP write_summ_terminal_ch.
   
   TRANSITION write_summ_terminal_ch
    WRITE summ(p1_integer,SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ no_money(TARGET_PID) FROM server2terminal_ch
    FROM NOW TO INF
   JUMP write_no_money_terminal_ch.
   
   TRANSITION write_no_money_terminal_ch
    WRITE no_money(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ correct_code(TARGET_PID) FROM server2terminal_ch
    FROM NOW TO INF
   JUMP write_correct_code_terminal_ch.
   
   TRANSITION write_correct_code_terminal_ch
    WRITE correct_code(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ wrong_code(TARGET_PID) FROM server2terminal_ch
    FROM NOW TO INF
   JUMP write_wrong_code_terminal_ch.
   
   TRANSITION write_wrong_code_terminal_ch
    WRITE wrong_code(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ check_code(p1_integer,p2_integer,TARGET_PID) FROM terminal2server_ch
    FROM NOW TO INF
   JUMP write_check_code_server_ch.
   
   TRANSITION write_check_code_server_ch
    WRITE check_code(p1_integer,p2_integer,SENDER) INTO buffer2server_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ check_summ(p1_integer,p2_integer,TARGET_PID) FROM terminal2server_ch
    FROM NOW TO INF
   JUMP write_check_summ_server_ch.
   
   TRANSITION write_check_summ_server_ch
    WRITE check_summ(p1_integer,p2_integer,SENDER) INTO buffer2server_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
   READ balance(p1_integer,TARGET_PID) FROM terminal2server_ch
    FROM NOW TO INF
   JUMP write_balance_server_ch.
   
   TRANSITION write_balance_server_ch
    WRITE balance(p1_integer,SENDER) INTO buffer2server_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ insert_summ(p1_integer,p2_integer,TARGET_PID) FROM terminal2server_ch
    FROM NOW TO INF
   JUMP write_insert_summ_server_ch.
   
   TRANSITION write_insert_summ_server_ch
    WRITE insert_summ(p1_integer,p2_integer,SENDER) INTO buffer2server_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ card(p1_integer,TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_card_terminal_ch.
   
   TRANSITION write_card_terminal_ch
    WRITE card(p1_integer,SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ press_start(TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_press_start_terminal_ch.
   
   TRANSITION write_press_start_terminal_ch
    WRITE press_start(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ insert_money(TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_insert_money_terminal_ch.
   
   TRANSITION write_insert_money_terminal_ch
    WRITE insert_money(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ get_money(TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_get_money_terminal_ch.
   
   TRANSITION write_get_money_terminal_ch
    WRITE get_money(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ get_balance(TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_get_balance_terminal_ch.
   
   TRANSITION write_get_balance_terminal_ch
    WRITE get_balance(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ done(TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_done_terminal_ch.
   
   TRANSITION write_done_terminal_ch
    WRITE done(SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ summ(p1_integer,TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_summ_terminal_ch.
   
   TRANSITION write_summ_terminal_ch
    WRITE summ(p1_integer,SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ code(p1_integer,TARGET_PID) FROM client2terminal_ch
    FROM NOW TO INF
   JUMP write_code_terminal_ch.
   
   TRANSITION write_code_terminal_ch
    WRITE code(p1_integer,SENDER) INTO buffer2terminal_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ card(p1_integer,TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_card_client_ch.
   
   TRANSITION write_card_client_ch
    WRITE card(p1_integer,SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ terminal_no_money(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_terminal_no_money_client_ch.
   
   TRANSITION write_terminal_no_money_client_ch
    WRITE terminal_no_money(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ no_money(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_no_money_client_ch.
   
   TRANSITION write_no_money_client_ch
    WRITE no_money(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ correct_card(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_correct_card_client_ch.
   
   TRANSITION write_correct_card_client_ch
    WRITE correct_card(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ limit(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_limit_client_ch.
   
   TRANSITION write_limit_client_ch
    WRITE limit(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ correct_code(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_correct_code_client_ch.
   
   TRANSITION write_correct_code_client_ch
    WRITE correct_code(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ wrong_card(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_wrong_card_client_ch.
   
   TRANSITION write_wrong_card_client_ch
    WRITE wrong_card(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ wrong_code(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_wrong_code_client_ch.
   
   TRANSITION write_wrong_code_client_ch
    WRITE wrong_code(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ ready_for_job(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_ready_for_job_client_ch.
   
   TRANSITION write_ready_for_job_client_ch
    WRITE ready_for_job(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ cheque(TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_cheque_client_ch.
   
   TRANSITION write_cheque_client_ch
    WRITE cheque(SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ money(p1_integer,TARGET_PID) FROM terminal2client_ch
    FROM NOW TO INF
   JUMP write_money_client_ch.
   
   TRANSITION write_money_client_ch
    WRITE money(p1_integer,SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ kill_cl(TARGET_PID) FROM client2queue_ch
    FROM NOW TO INF
   JUMP write_kill_cl_queue_ch.
   
   TRANSITION write_kill_cl_queue_ch
    WRITE kill_cl(SENDER) INTO buffer2queue_ch
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ init(p1_integer,p2_integer,p3_integer,p4_PId,p5_integer,TARGET_PID) FROM queue2client_ch
    FROM NOW TO INF
   JUMP write_init_client_ch.
   
   TRANSITION write_init_client_ch
    WRITE init(p1_integer,p2_integer,p3_integer,p4_PId,p5_integer,SENDER) INTO buffer2client_ch[TARGET_PID]
    FROM NOW TO NOW
   JUMP read_signal.
   
   TRANSITION read_signal
    READ init_cl(p1_integer,p2_integer,p3_integer,p4_integer,TARGET_PID) FROM ENV2queue_ch
    FROM NOW TO INF
   JUMP write_init_cl_queue_ch.
   
   TRANSITION write_init_cl_queue_ch
    WRITE init_cl(p1_integer,p2_integer,p3_integer,p4_integer,SENDER) INTO buffer2queue_ch
    FROM NOW TO NOW
   JUMP read_signal.
  END; /* buffer */ 
  
  queue: PROCESS/*116*/
   TYPE integer_array IS
    integer ARRAY OF integer.
   TYPE char_array IS
    integer ARRAY OF STR.
   TYPE PId_array IS
    integer ARRAY OF PId.
	
   TYPE arr_pid IS
    PId ARRAY OF integer.
	
   TYPE arr IS
    integer ARRAY OF PId.
	
   PR VAR SENDER_PID OF PId.
   PR VAR terminal_count OF integer.
   PR VAR terminal_num OF integer.
   PR VAR MAX_TERMINAL OF integer.
   PR VAR cl_summ OF integer.
   PR VAR cl_card_num OF integer.
   PR VAR cl_code OF integer.
   PR VAR cl_operation OF integer.
   PR VAR count OF integer.
   PR VAR pid_to_idx OF arr_pid.
   PR VAR terminal_inst OF arr.
   
   PR VAR _sig_p1 OF integer_array.
   PR VAR _sig_p2 OF integer_array.
   PR VAR _sig_p3 OF integer_array.
   PR VAR _sig_p4 OF integer_array.
   PR VAR _sender_list OF PId_array.
   PR VAR _signal_list OF char_array.
   PR VAR _i_size OF integer.
   PR VAR _i_current OF integer.
   PR VAR _temp_cl_summ OF integer.
   PR VAR _temp_cl_card_num OF integer.
   PR VAR _temp_cl_code OF integer.
   PR VAR _temp_cl_operation OF integer.

   PROCEDURE add_signal(p1, p2, p3, p4)
	   _sender_list[_i_size]:=SENDER_PID;
	   _sig_p1[_i_size]:=p1;
	   _sig_p2[_i_size]:=p2;
	   _sig_p3[_i_size]:=p3;
	   _sig_p4[_i_size]:=p4;
	   _i_size:=_i_size+1;
   END PROCEDURE.

   PROCEDURE shift_arrays(size, current)
      LOCAL VAR last;
    
      last := current;
      WHILE (last < size - 1)  DO
         _signal_list[last]:=_signal_list[last+1];
         _sender_list[last]:=_sender_list[last+1];
         _sig_p1[last]:=_sig_p1[last+1];
         _sig_p2[last]:=_sig_p2[last+1];
         _sig_p3[last]:=_sig_p3[last+1];
         _sig_p4[last]:=_sig_p4[last+1];
         last:=last+1;
      OD;
      size:=size-1;
      _signal_list[size]:=NULL;
      _sender_list[size]:=NULL;
      _sig_p1[size]:=0;
      _sig_p2[size]:=0;
      _sig_p3[size]:=0;
      _sig_p4[size]:=0;
    END PROCEDURE. /* shift_arrays(size, current)) */
   
   TRANSITION Start__rl/*112*/
   EXE count:=0;/*113*/
   terminal_count:=0;/*114*/
   MAX_TERMINAL:=3;/*115*/
   terminal_inst[1]:=Null;/*116*/
   terminal_inst[2]:=Null;/*117*/
   terminal_inst[3]:=Null;/*118*/
   _i_size:=0;
   _i_current:=0;
   FROM NOW TO NOW
   JUMP look__start.
   
   TRANSITION look__start
   EXE _i_current:=0;
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*121*/
   READ init_terminal(SENDER_PID) FROM buffer2queue_ch
   FROM NOW TO INF
   JUMP look__read_1.

   TRANSITION look__read_1 
   EXE _signal_list[_i_size]:="init_terminal";
	   CALL add_signal(0,0,0,0);
   JUMP look__rl.
 
  TRANSITION look__rl/*121*/
   READ ready(SENDER_PID) FROM buffer2queue_ch/*154*/
   FROM NOW TO INF
   JUMP look__read_2.

   TRANSITION look__read_2 
   EXE _signal_list[_i_size]:="ready";
		CALL add_signal(0,0,0,0);
   FROM NOW TO INF
   JUMP look__rl.
 
  TRANSITION look__rl/*121*/
   READ init_cl(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_cl_operation, SENDER_PID) FROM buffer2queue_ch/*196*/
   FROM NOW TO INF
   JUMP look__read_3.
   
   TRANSITION look__read_3 
   EXE _signal_list[_i_size]:="init_cl";
		CALL add_signal(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_cl_operation);
   FROM NOW TO INF
   JUMP look__rl. 

   TRANSITION look__rl/*121*/
   READ kill_cl(SENDER_PID) FROM buffer2queue_ch/*217*/
   FROM NOW TO INF
   JUMP look__read_4.
   
   TRANSITION look__read_4 
   EXE _signal_list[_i_size]:="kill_cl";
		CALL add_signal(0,0,0,0);
   FROM NOW TO INF
   JUMP look__rl.  
   
   TRANSITION look__rl/*121*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "init_terminal")/*123*/
	EXE terminal_count:=terminal_count+1;/*125*/
		pid_to_idx[_sender_list[_i_current]]:=terminal_count;/*126*/
		terminal_inst[terminal_count]:=_sender_list[_i_current];/*127*/
		terminal_num:=pid_to_idx[_sender_list[_i_current]];/*128*/
		CALL shift_arrays(_i_size, _i_current);
   FROM NOW TO NOW
   JUMP look__start./*129*/
   
   TRANSITION look__rl/*121*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "ready")/*130*/
   EXE 	terminal_num:=pid_to_idx[_sender_list[_i_current]];/*132*/
		CALL shift_arrays(_i_size, _i_current);
   FROM NOW TO NOW
   JUMP next_client__start./*133*/

   TRANSITION look__rl/*121*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "init_cl")/*122*/
   EXE _i_current := _i_current+1;
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*121*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "kill_cl")/*134*/
   EXE 	count:=count-1;/*136*/
		CALL shift_arrays(_i_size, _i_current);
   FROM NOW TO INF
   JUMP look__start./*137*/

   TRANSITION next_client__start
   EXE _i_current:=0;
   FROM NOW TO INF
   JUMP next_client__rl.
   
   TRANSITION next_client__rl/*140*/
   READ init_terminal(SENDER_PID) FROM buffer2queue_ch
   FROM NOW TO INF
   JUMP next_client__read_1.

   TRANSITION next_client__read_1 
   EXE _signal_list[_i_size]:="init_terminal";
	   CALL add_signal(0,0,0,0);
   JUMP next_client__rl.
 
  TRANSITION next_client__rl/*140*/
   READ ready(SENDER_PID) FROM buffer2queue_ch
   FROM NOW TO INF
   JUMP next_client__read_2.

   TRANSITION next_client__read_2 
   EXE _signal_list[_i_size]:="ready";
		CALL add_signal(0,0,0,0);
   FROM NOW TO INF
   JUMP next_client__rl.
 
  TRANSITION next_client__rl/*140*/
   READ init_cl(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_cl_operation, SENDER_PID) FROM buffer2queue_ch
   FROM NOW TO INF
   JUMP next_client__read_3.
   
   TRANSITION next_client__read_3 
   EXE _signal_list[_i_size]:="init_cl";
		CALL add_signal(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_cl_operation);
   FROM NOW TO INF
   JUMP next_client__rl. 

   TRANSITION next_client__rl/*140*/
   READ kill_cl(SENDER_PID) FROM buffer2queue_ch
   FROM NOW TO INF
   JUMP next_client__read_4.
   
   TRANSITION next_client__read_4 
   EXE _signal_list[_i_size]:="kill_cl";
		CALL add_signal(0,0,0,0);
   FROM NOW TO INF
   JUMP next_client__rl.  

   TRANSITION next_client__rl/*140*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "init_terminal")/*141*/
   EXE _i_current := _i_current+1;
   FROM NOW TO NOW
   JUMP next_client__rl.
   
   TRANSITION next_client__rl/*140*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "ready")/*142*/
   EXE _i_current := _i_current+1;
   FROM NOW TO NOW
   JUMP next_client__rl.

   TRANSITION next_client__rl/*140*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "init_cl")/*143*/
   EXE  cl_summ := _sig_p1[_i_current];
	    cl_card_num := _sig_p2[_i_current];
		cl_code := _sig_p3[_i_current];
		cl_operation := _sig_p4[_i_current];
		CALL shift_arrays(_i_size, _i_current);
   FROM NOW TO INF
   JUMP next_client__X1.
   
   TRANSITION next_client__X1
   CREATE PROCESS client/*144*/
   FROM NOW TO INF
   JUMP next_client__X2.

   TRANSITION next_client__X2
   WRITE init(cl_summ,cl_card_num,cl_code,terminal_inst[terminal_num],cl_operation, OFFSPRING) INTO queue2client_ch/*145*/
   FROM NOW TO INF
   JUMP next_client__X3.

   TRANSITION next_client__X3
   EXE count:=count+1;/*146*/
   FROM NOW TO NOW
   JUMP look__start./*147*/

   TRANSITION next_client__rl/*140*/
   WHEN(_i_size<>0 AND _i_current< _i_size AND _signal_list[_i_current] = "kill_cl")/*149*/
   EXE 	count:=count-1;/*151*/
		CALL shift_arrays(_i_size, _i_current);
   FROM NOW TO INF
   JUMP next_client__start./*152*/
  END; /* queue */ 
  
  server: PROCESS/*377*/
   TYPE int_arr IS
    integer ARRAY OF integer.
   PR VAR SENDER_PID OF PId.
   PR VAR cl_summ OF integer.
   PR VAR cl_card_num OF integer.
   PR VAR cl_code OF integer.
   
   TRANSITION Start__rl/*387*/
   EXE code_table[1]:=12;/*388*/
   code_table[2]:=23;/*389*/
   code_table[3]:=34;/*390*/
   summ_table[1]:=10;/*391*/
   summ_table[2]:=40;/*392*/
   summ_table[3]:=80;/*393*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION main__rl/*425*/
   READ check_code(cl_card_num,cl_code,SENDER_PID) FROM buffer2server_ch/*429*/
   FROM NOW TO INF
   JUMP begin_decision__1.
   
   TRANSITION begin_decision__1/*430*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP main__1.
   
   TRANSITION main__1/*425*/
   WHEN (code_table[cl_card_num]<>cl_code)/*432*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP main__1__X1.
   
   TRANSITION main__1__X1/*425*/
   WRITE wrong_code(SENDER_PID) INTO server2terminal_ch/*432*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION main__1/*425*/
   WHEN (NOT (code_table[cl_card_num]<>cl_code))/*432*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP main__1__X2.
   
   TRANSITION main__1__X2/*425*/
   WRITE correct_code(SENDER_PID) INTO server2terminal_ch/*435*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION end_decision__1/*425*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION main__rl/*425*/
   READ check_summ(cl_card_num,cl_summ,SENDER_PID) FROM buffer2server_ch/*438*/
   FROM NOW TO INF
   JUMP begin_decision__2.
   
   TRANSITION begin_decision__2/*439*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP main__2.
   
   TRANSITION main__2/*425*/
   WHEN (summ_table[cl_card_num]<cl_summ)/*442*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP main__2__X3.
   
   TRANSITION main__2__X3/*425*/
   WRITE no_money(SENDER_PID) INTO server2terminal_ch/*442*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION main__2/*425*/
   WHEN (NOT (summ_table[cl_card_num]<cl_summ))/*442*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP main__2__X4.
   
   TRANSITION main__2__X4/*425*/
   WRITE summ(cl_summ,SENDER_PID) INTO server2terminal_ch/*445*/
   FROM NOW TO NOW
   JUMP main__2__X5.
   
   TRANSITION main__2__X5/*425*/
   EXE summ_table[cl_card_num]:=summ_table[cl_card_num]-cl_summ;/*446*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION end_decision__2/*425*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION main__rl/*467*/
   READ balance(cl_card_num,SENDER_PID) FROM buffer2server_ch/*491*/
   FROM NOW TO INF
   JUMP main__X6.
   
   TRANSITION main__X6/*467*/
   WRITE summ(summ_table[cl_card_num],SENDER_PID) INTO server2terminal_ch/*492*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION main__rl/*467*/
   READ insert_summ(cl_card_num,cl_summ,SENDER_PID) FROM buffer2server_ch/*494*/
   FROM NOW TO INF
   JUMP main__X7.
   
   TRANSITION main__X7/*467*/
   EXE summ_table[cl_card_num]:=summ_table[cl_card_num]+cl_summ;/*495*/
   FROM NOW TO NOW
   JUMP main__X8.
   
   TRANSITION main__X8/*467*/
   WRITE summ(summ_table[cl_card_num],SENDER_PID) INTO server2terminal_ch/*496*/
   FROM NOW TO NOW
   JUMP main__rl.
  END; /* server */ 
  
  client: PROCESS(0,250)/*224*/
   PR VAR SENDER_PID OF PId.
   PR VAR terminal_pid OF PId.
   PR VAR cl_summ OF integer.
   PR VAR cl_card_num OF integer.
   PR VAR cl_code OF integer.
   PR VAR g_summ OF integer.
   PR VAR is_terminal_ready OF integer.
   PR VAR cl_operation OF integer.
   
   PR VAR _temp_terminal_pid OF PId.
   PR VAR _temp_cl_summ OF integer.
   PR VAR _temp_cl_card_num OF integer.
   PR VAR _temp_cl_code OF integer.
   PR VAR _temp_g_summ OF integer.
   PR VAR _temp_cl_operation OF integer.
   
   TRANSITION Start__rl/*232*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ init(cl_summ,cl_card_num,cl_code,terminal_pid,cl_operation,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__X1.
   
   TRANSITION init_client__X1/*233*/
   EXE wants_summ[SELF]:=cl_summ;/*237*/
   FROM NOW TO NOW
   JUMP init_client__X2.
   
   TRANSITION init_client__X2/*233*/
   WRITE press_start(terminal_pid) INTO client2terminal_ch/*238*/
   FROM NOW TO NOW
   JUMP wait_terminal__rl.

/*****/   
   TRANSITION init_client__rl/*233*/
   READ ready_for_job(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ correct_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ wrong_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.

   TRANSITION init_client__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ correct_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ money(_temp_g_summ,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ cheque(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ limit(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ terminal_no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.

   TRANSITION init_client__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
   
   TRANSITION init_client__rl/*233*/
   READ card(_temp_cl_card_num,SENDER_PID)  FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP init_client__rl.
/*****/ 
   TRANSITION wait_terminal__rl/*239*/
   READ ready_for_job(SENDER_PID) FROM buffer2client_ch/*243*/
   FROM NOW TO INF
   JUMP wait_terminal__X1.
   
   TRANSITION wait_terminal__X1/*239*/
   WRITE card(cl_card_num,terminal_pid) INTO client2terminal_ch/*244*/
   FROM NOW TO NOW
   JUMP look__rl.
    
/*****/   
   TRANSITION wait_terminal__rl/*233*/
   READ init(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_terminal_pid,_temp_cl_operation, SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ correct_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ wrong_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.

   TRANSITION wait_terminal__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ correct_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ money(_temp_g_summ,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ cheque(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ limit(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ terminal_no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.

   TRANSITION wait_terminal__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
   
   TRANSITION wait_terminal__rl/*233*/
   READ card(_temp_cl_card_num,SENDER_PID)  FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP wait_terminal__rl.
  /*****/ 
  
   TRANSITION look__rl/*245*/
   READ correct_card(SENDER_PID) FROM buffer2client_ch/*249*/
   FROM NOW TO INF
   JUMP look__X1.
   
   TRANSITION look__X1/*245*/
   WRITE code(cl_code,terminal_pid) INTO client2terminal_ch/*250*/
   FROM NOW TO NOW
   JUMP look__rl.
   
   TRANSITION look__rl/*245*/
   READ wrong_card(SENDER_PID) FROM buffer2client_ch/*253*/
   FROM NOW TO INF
   JUMP look__X2.
   
   TRANSITION look__X2/*245*/
   WRITE done(terminal_pid) INTO client2terminal_ch/*254*/
   FROM NOW TO NOW
   JUMP look__X3.
   
   TRANSITION look__X3/*245*/
   WRITE kill_cl(Null) INTO client2queue_ch/*255*/
   FROM NOW TO NOW
   JUMP no_card__rl.
   
   TRANSITION no_card__rl/*256*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP no_card__stop__rl.
   
   TRANSITION look__rl/*245*/
   READ correct_code(SENDER_PID) FROM buffer2client_ch/*258*/
   FROM NOW TO INF
   JUMP begin_decision__1.
   
   TRANSITION begin_decision__1/*264*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP look__1.
   
   TRANSITION look__1/*250*/
   WHEN (cl_operation=1)/*273*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP look__1__X4.
   
   TRANSITION look__1__X4/*250*/
   WRITE get_balance(terminal_pid) INTO client2terminal_ch/*266*/
   FROM NOW TO NOW
   JUMP get_cheque__rl.
   
   TRANSITION look__1/*250*/
   WHEN (cl_operation=2)/*273*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP look__1__X5.
   
   TRANSITION look__1__X5/*250*/
   WRITE get_money(terminal_pid) INTO client2terminal_ch/*269*/
   FROM NOW TO NOW
   JUMP look__1__X6.
   
   TRANSITION look__1__X6/*250*/
   WRITE summ(cl_summ,terminal_pid) INTO client2terminal_ch/*270*/
   FROM NOW TO NOW
   JUMP get_money__rl.

   TRANSITION look__1/*250*/
   WHEN (cl_operation=3)/*273*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP look__1__X7.
   
   TRANSITION look__1__X7/*250*/
   WRITE insert_money(terminal_pid) INTO client2terminal_ch/*273*/
   FROM NOW TO NOW
   JUMP look__1__X8.
   
   TRANSITION look__1__X8/*250*/
   WRITE summ(cl_summ,terminal_pid) INTO client2terminal_ch/*274*/
   FROM NOW TO NOW
   JUMP get_cheque__rl.
   
   TRANSITION get_money__rl/*260*/
   READ money(g_summ,SENDER_PID) FROM buffer2client_ch/*269*/
   FROM NOW TO INF
   JUMP get_money__X1.
   
   TRANSITION look__rl/*245*/
   READ wrong_code(SENDER_PID) FROM buffer2client_ch/*262*/
   FROM NOW TO INF
   JUMP look__X5.
   
   TRANSITION look__X5/*245*/
   WRITE done(terminal_pid) INTO client2terminal_ch/*263*/
   FROM NOW TO NOW
   JUMP look__X6.
   
   TRANSITION look__X6/*245*/
   WRITE kill_cl(Null) INTO client2queue_ch/*264*/
   FROM NOW TO NOW
   JUMP no_code__rl.
   
   TRANSITION no_code__rl/*265*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP no_code__stop__rl.
   
/*****/  
   TRANSITION look__rl/*233*/
   READ init(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_terminal_pid,_temp_cl_operation, SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*233*/
   READ ready_for_job(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*233*/
   READ money(_temp_g_summ,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*233*/
   READ cheque(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*233*/
   READ limit(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*233*/
   READ terminal_no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.

   TRANSITION look__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
   
   TRANSITION look__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP look__rl.
  /*****/ 
  
   TRANSITION get_money__X1/*260*/
   EXE gotten_summ[SELF]:=g_summ;/*270*/
   FROM NOW TO NOW
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*271*/
   READ cheque(SENDER_PID) FROM buffer2client_ch/*287*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_money__rl/*260*/
   READ limit(SENDER_PID) FROM buffer2client_ch/*272*/
   FROM NOW TO INF
   JUMP get_money__X2.
   
   TRANSITION get_money__X2/*260*/
   WRITE done(terminal_pid) INTO client2terminal_ch/*273*/
   FROM NOW TO NOW
   JUMP get_money__X3.
   
   TRANSITION get_money__X3/*260*/
   WRITE kill_cl(Null) INTO client2queue_ch/*274*/
   FROM NOW TO NOW
   JUMP limit_money__rl.
   
   TRANSITION limit_money__rl/*275*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP limit_money__stop__rl.
   
   TRANSITION get_money__rl/*260*/
   READ terminal_no_money(SENDER_PID) FROM buffer2client_ch/*276*/
   FROM NOW TO INF
   JUMP get_money__X4.
   
   TRANSITION get_money__X4/*260*/
   WRITE done(terminal_pid) INTO client2terminal_ch/*277*/
   FROM NOW TO NOW
   JUMP get_money__X5.
   
   TRANSITION get_money__X5/*260*/
   WRITE kill_cl(Null) INTO client2queue_ch/*278*/
   FROM NOW TO NOW
   JUMP term_no_money__rl.
   
   TRANSITION term_no_money__rl/*279*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP term_no_money__stop__rl.
   
   TRANSITION get_money__rl/*260*/
   READ no_money(SENDER_PID) FROM buffer2client_ch/*280*/
   FROM NOW TO INF
   JUMP get_money__X6.
   
   TRANSITION get_money__X6/*260*/
   WRITE done(terminal_pid) INTO client2terminal_ch/*281*/
   FROM NOW TO NOW
   JUMP get_money__X7.
   
   TRANSITION get_money__X7/*260*/
   WRITE kill_cl(Null) INTO client2queue_ch/*282*/
   FROM NOW TO NOW
   JUMP cl_no_money__rl.
   
   TRANSITION cl_no_money__rl/*283*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP cl_no_money__stop__rl.
   
/*****/   
   TRANSITION get_money__rl/*233*/
   READ init(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_terminal_pid,_temp_cl_operation, SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
   
   TRANSITION get_money__rl/*233*/
   READ ready_for_job(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
   
   TRANSITION get_money__rl/*233*/
   READ correct_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
   
   TRANSITION get_money__rl/*233*/
   READ wrong_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.

   TRANSITION get_money__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
   
   TRANSITION get_money__rl/*233*/
   READ correct_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
   
   TRANSITION get_money__rl/*233*/
   READ cheque(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
   
   TRANSITION get_money__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_money__rl.
  /*****/    
  
  /*****/   
   TRANSITION get_cheque__rl/*233*/
   READ init(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_terminal_pid,_temp_cl_operation, SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ ready_for_job(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ correct_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ wrong_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.

   TRANSITION get_cheque__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ correct_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ money(_temp_g_summ,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
 
   TRANSITION get_cheque__rl/*233*/
   READ limit(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ terminal_no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.

   TRANSITION get_cheque__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
   
   TRANSITION get_cheque__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_cheque__rl.
  /*****/ 
  
   TRANSITION get_card__rl/*288*/
   READ card(cl_card_num,SENDER_PID) FROM buffer2client_ch/*292*/
   FROM NOW TO INF
   JUMP get_card__X1.
   
   TRANSITION get_card__X1/*288*/
   WRITE done(terminal_pid) INTO client2terminal_ch/*293*/
   FROM NOW TO NOW
   JUMP get_card__X2.
   
   TRANSITION get_card__X2/*288*/
   WRITE kill_cl(Null) INTO client2queue_ch/*294*/
   FROM NOW TO NOW
   JUMP good__rl.
 /*****/   
   TRANSITION get_card__rl/*233*/
   READ init(_temp_cl_summ,_temp_cl_card_num,_temp_cl_code,_temp_terminal_pid,_temp_cl_operation, SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ ready_for_job(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ correct_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ wrong_card(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.

   TRANSITION get_card__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ correct_code(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ money(_temp_g_summ,SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ cheque(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ limit(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
   
   TRANSITION get_card__rl/*233*/
   READ terminal_no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.

   TRANSITION get_card__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2client_ch/*236*/
   FROM NOW TO INF
   JUMP get_card__rl.
  /*****/    
   TRANSITION good__rl/*295*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP good__stop__rl.
   
   TRANSITION no_code__stop__rl/*265*/
   WHEN TRUE
   STOP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION no_card__stop__rl/*256*/
   WHEN TRUE
   STOP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION good__stop__rl/*295*/
   WHEN TRUE
   STOP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION cl_no_money__stop__rl/*283*/
   WHEN TRUE
   STOP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION limit_money__stop__rl/*275*/
   WHEN TRUE
   STOP
   FROM NOW TO NOW
   JUMP end_of_process.
   
   TRANSITION term_no_money__stop__rl/*279*/
   WHEN TRUE
   STOP
   FROM NOW TO NOW
   JUMP end_of_process.
  END; /* client */ 
  
  terminal: PROCESS(6,6)/*331*/
   PR VAR SENDER_PID OF PId.
   PR VAR client_pid OF PId.
   PR VAR one_time_limit OF integer.
   PR VAR terminal_summ OF integer.
   PR VAR cl_summ OF integer.
   PR VAR cl_card_num OF integer.
   PR VAR cl_code OF integer.
   PR VAR output_summ OF integer.

   PR VAR _temp_cl_summ OF integer.
   PR VAR _temp_cl_card_num OF integer.
   PR VAR _temp_cl_code OF integer.
   PR VAR _temp_output_summ OF integer.
   
   TRANSITION Start__rl/*336*/
   EXE terminal_summ:=100;/*337*/
   one_time_limit:=90;/*338*/
   FROM NOW TO NOW
   JUMP Start__X1.
   
   TRANSITION Start__X1/*336*/
   WRITE init_terminal(Null) INTO terminal2queue_ch/*339*/
   FROM NOW TO NOW
   JUMP Start__X2.
   
   TRANSITION Start__X2/*336*/
   WRITE ready(Null) INTO terminal2queue_ch/*340*/
   FROM NOW TO NOW
   JUMP main__rl.
   
   TRANSITION main__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP main__X1.
   
   TRANSITION main__X1/*341*/
   EXE cl_summ:=0;/*345*/
   cl_card_num:=0;/*346*/
   cl_code:=0;/*347*/
   FROM NOW TO NOW
   JUMP main__X2.
   
   TRANSITION main__X2/*341*/
   WRITE ready_for_job(SENDER_PID) INTO terminal2client_ch/*348*/
   FROM NOW TO NOW
   JUMP wait_for_card__rl.
   /*****/
   TRANSITION main__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.

   TRANSITION main__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.

   TRANSITION main__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.

   TRANSITION main__rl/*233*/
   READ summ(_temp_cl_summ,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.
   
   TRANSITION main__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.
   
   TRANSITION main__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.
   
   TRANSITION main__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.  

   TRANSITION main__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.  

   TRANSITION main__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.  

   TRANSITION main__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP main__rl.  
 /*****/
   TRANSITION wait_for_card__rl/*349*/
   READ card(cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*353*/
   FROM NOW TO INF
   JUMP wait_for_card__X1.
   
   TRANSITION wait_for_card__X1/*349*/
   EXE client_pid:=SENDER_PID;/*354*/
   FROM NOW TO NOW
   JUMP wait_for_card__X2.
   
   TRANSITION wait_for_card__X2/*349*/
   WRITE correct_card(client_pid) INTO terminal2client_ch/*355*/
   FROM NOW TO NOW
   JUMP wait_for_code__rl.
 
   /*****/
   TRANSITION wait_for_card__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.

   TRANSITION wait_for_card__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.

   TRANSITION wait_for_card__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.

   TRANSITION wait_for_card__rl/*233*/
   READ summ(_temp_cl_summ,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.
   
   TRANSITION wait_for_card__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.
   
   TRANSITION wait_for_card__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.
   
   TRANSITION wait_for_card__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.
  
   TRANSITION wait_for_card__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.  

   TRANSITION wait_for_card__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.  

   TRANSITION wait_for_card__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_card__rl.  
 /*****/
   
   TRANSITION wait_for_code__rl/*356*/
   READ code(cl_code,SENDER_PID) FROM buffer2terminal_ch/*360*/
   FROM NOW TO INF
   JUMP wait_for_code__X1.
   
   TRANSITION wait_for_code__X1/*356*/
   WRITE check_code(cl_card_num,cl_code,Null) INTO terminal2server_ch/*361*/
   FROM NOW TO NOW
   JUMP check_code__rl.
 
/*****/
   TRANSITION wait_for_code__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.
   
   TRANSITION wait_for_code__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.

   TRANSITION wait_for_code__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.

   TRANSITION wait_for_code__rl/*233*/
   READ summ(_temp_cl_summ,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.
   
   TRANSITION wait_for_code__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.
   
   TRANSITION wait_for_code__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.
   
   TRANSITION wait_for_code__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.  

   TRANSITION wait_for_code__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.  

   TRANSITION wait_for_code__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl.  

   TRANSITION wait_for_code__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_code__rl. 
 /*****/
   
   TRANSITION check_code__rl/*362*/
   READ correct_code(SENDER_PID) FROM buffer2terminal_ch/*366*/
   FROM NOW TO INF
   JUMP check_code__X1.
   
   TRANSITION check_code__X1/*362*/
   WRITE correct_code(client_pid) INTO terminal2client_ch/*367*/
   FROM NOW TO NOW
    JUMP wait_for_operation__rl.
   
   TRANSITION wait_for_operation__rl/*386*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*395*/
   FROM NOW TO INF
   JUMP wait_for_operation__X1.

   TRANSITION wait_for_operation__X1/*386*/
   WRITE balance(cl_card_num,Null) INTO terminal2server_ch/*396*/
   FROM NOW TO NOW
   JUMP wait_for_balance__rl.
   
   TRANSITION wait_for_operation__rl/*386*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*398*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.  

   TRANSITION wait_for_operation__rl/*386*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*400*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.

   /*****/
   TRANSITION wait_for_operation__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.
   
   TRANSITION wait_for_operation__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.

   TRANSITION wait_for_operation__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.

   TRANSITION wait_for_operation__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.

   TRANSITION wait_for_operation__rl/*233*/
   READ summ(_temp_cl_summ,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.
   
   TRANSITION wait_for_operation__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.
   
   TRANSITION wait_for_operation__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.
   
   TRANSITION wait_for_operation__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_operation__rl.  
  /*****/

   TRANSITION wait_for_balance__rl/*397*/
   READ summ(output_summ,SENDER_PID) FROM buffer2terminal_ch/*411*/
   FROM NOW TO INF
   JUMP wait_for_balance__X1.

   TRANSITION wait_for_balance__X1/*397*/
   WRITE cheque(client_pid) INTO terminal2client_ch/*412*/
   FROM NOW TO NOW
   JUMP wait_for_balance__X2.

   TRANSITION wait_for_balance__X2/*397*/
   WRITE card(cl_card_num,client_pid) INTO terminal2client_ch/*413*/
   FROM NOW TO NOW
   JUMP satisfaction__rl.

  /*****/
   TRANSITION wait_for_balance__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.
   
   TRANSITION wait_for_balance__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.

   TRANSITION wait_for_balance__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.

   TRANSITION wait_for_balance__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.
   
   TRANSITION wait_for_balance__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.
   
   TRANSITION wait_for_balance__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.
   
   TRANSITION wait_for_balance__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl. 

   TRANSITION wait_for_balance__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.  

   TRANSITION wait_for_balance__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.  

   TRANSITION wait_for_balance__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_balance__rl.  
  /*****/


   TRANSITION wait_for_insert__rl/*401*/
   READ summ(cl_summ,SENDER_PID) FROM buffer2terminal_ch/*405*/
   FROM NOW TO INF
   JUMP wait_for_insert__X1.
   
   TRANSITION wait_for_insert__X1/*401*/
   WRITE insert_summ(cl_card_num,cl_summ,Null) INTO terminal2server_ch/*406*/
   FROM NOW TO NOW
   JUMP wait_for_balance__rl.

  /*****/
   TRANSITION wait_for_insert__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.
   
   TRANSITION wait_for_insert__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.

   TRANSITION wait_for_insert__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.

   TRANSITION wait_for_insert__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.
   
   TRANSITION wait_for_insert__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.
   
   TRANSITION wait_for_insert__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.
   
   TRANSITION wait_for_insert__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl. 

   TRANSITION wait_for_insert__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.  

   TRANSITION wait_for_insert__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.  

   TRANSITION wait_for_insert__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_insert__rl.  
  /*****/

   TRANSITION wait_for_summ__rl/*368*/
   READ summ(cl_summ,SENDER_PID) FROM buffer2terminal_ch/*376*/
   FROM NOW TO INF
   JUMP begin_decision__1.
   
   TRANSITION check_code__rl/*362*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*370*/
   FROM NOW TO INF
   JUMP check_code__X2.
   
   TRANSITION check_code__X2/*362*/
   WRITE wrong_code(client_pid) INTO terminal2client_ch/*371*/
   FROM NOW TO NOW
   JUMP satisfaction__rl.
  /*****/
   TRANSITION check_code__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP check_code__rl.
   
   TRANSITION check_code__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.

   TRANSITION check_code__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.

   TRANSITION check_code__rl/*233*/
   READ summ(_temp_cl_summ,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.

   TRANSITION check_code__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.
   
   TRANSITION check_code__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.  

   TRANSITION check_code__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.  

   TRANSITION check_code__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl.  

   TRANSITION check_code__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_code__rl. 
/*****/
   
   TRANSITION satisfaction__rl/*372*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*402*/
   FROM NOW TO INF
   JUMP satisfaction__X1.
   
 /*****/
   TRANSITION satisfaction__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP satisfaction__rl.
   
   TRANSITION satisfaction__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.

   TRANSITION satisfaction__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.

   TRANSITION satisfaction__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.

   TRANSITION satisfaction__rl/*233*/
   READ summ(_temp_cl_summ,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.
   
   TRANSITION satisfaction__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.
   
   TRANSITION satisfaction__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.  

   TRANSITION satisfaction__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.  

   TRANSITION satisfaction__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl.  

   TRANSITION satisfaction__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP satisfaction__rl. 
   /*****/   
   TRANSITION begin_decision__1/*377*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP wait_for_summ__1.
   
   TRANSITION wait_for_summ__1/*368*/
   WHEN (cl_summ>terminal_summ)/*382*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP wait_for_summ__1__X1.
   
   TRANSITION wait_for_summ__1__X1/*368*/
   WRITE terminal_no_money(client_pid) INTO terminal2client_ch/*379*/
   FROM NOW TO NOW
   JUMP satisfaction__rl.
   
   TRANSITION wait_for_summ__1/*368*/
   WHEN (cl_summ>one_time_limit)/*382*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP wait_for_summ__1__X2.
   
   TRANSITION wait_for_summ__1__X2/*368*/
   WRITE limit(client_pid) INTO terminal2client_ch/*382*/
   FROM NOW TO NOW
   JUMP wait_for_summ__1.
   
   TRANSITION wait_for_summ__1/*368*/
   WHEN (NOT ((cl_summ>terminal_summ) OR (cl_summ>one_time_limit)))
   /*382*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP wait_for_summ__1__X3.
   
   TRANSITION wait_for_summ__1__X3/*368*/
   WRITE check_summ(cl_card_num,cl_summ,Null) INTO terminal2server_ch/*384*/
   FROM NOW TO NOW
   JUMP check_summ__rl.
   
   TRANSITION check_summ__rl/*385*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*390*/
   FROM NOW TO INF
   JUMP check_summ__X1.
   
   TRANSITION end_decision__1/*368*/
   EXE SKIP
   FROM NOW TO NOW
   JUMP end_of_process.
   
/*****/
   TRANSITION wait_for_summ__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.
   
   TRANSITION wait_for_summ__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.

   TRANSITION wait_for_summ__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.

   TRANSITION wait_for_summ__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.
   
   TRANSITION wait_for_summ__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.
   
   TRANSITION wait_for_summ__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.
   
   TRANSITION wait_for_summ__rl/*233*/
   READ no_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.  

   TRANSITION wait_for_summ__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.  

   TRANSITION wait_for_summ__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl.  

   TRANSITION wait_for_summ__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP wait_for_summ__rl. 
   /*****/  
   
   TRANSITION check_summ__X1/*385*/
   WRITE no_money(client_pid) INTO terminal2client_ch/*391*/
   FROM NOW TO NOW
   JUMP satisfaction__rl.
   
   TRANSITION check_summ__rl/*385*/
   READ summ(output_summ,SENDER_PID) FROM buffer2terminal_ch/*393*/
   FROM NOW TO INF
   JUMP check_summ__X2.
   
   TRANSITION check_summ__X2/*385*/
   EXE terminal_summ:=terminal_summ-output_summ;/*394*/
   FROM NOW TO NOW
   JUMP check_summ__X3.
   
   TRANSITION check_summ__X3/*385*/
   WRITE money(output_summ,client_pid) INTO terminal2client_ch/*395*/
   FROM NOW TO NOW
   JUMP check_summ__X4.
   
   TRANSITION check_summ__X4/*385*/
   WRITE cheque(client_pid) INTO terminal2client_ch/*396*/
   FROM NOW TO NOW
   JUMP check_summ__X5.
   
   TRANSITION check_summ__X5/*385*/
   WRITE card(cl_card_num,client_pid) INTO terminal2client_ch/*397*/
   FROM NOW TO NOW
   JUMP satisfaction__rl.
   
   /*****/
   TRANSITION check_summ__rl/*341*/
   READ press_start(SENDER_PID) FROM buffer2terminal_ch/*344*/
   FROM NOW TO INF
   JUMP check_summ__rl.
   
   TRANSITION check_summ__rl/*233*/
   READ  card(_temp_cl_card_num,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.

   TRANSITION check_summ__rl/*233*/
   READ  code(_temp_cl_code,SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.

   TRANSITION check_summ__rl/*233*/
   READ  correct_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.
   
   TRANSITION check_summ__rl/*233*/
   READ wrong_code(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.
   
   TRANSITION check_summ__rl/*233*/
   READ done(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.

   TRANSITION check_summ__rl/*233*/
   READ get_balance(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.  

   TRANSITION check_summ__rl/*233*/
   READ get_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl.  

   TRANSITION check_summ__rl/*233*/
   READ insert_money(SENDER_PID) FROM buffer2terminal_ch/*236*/
   FROM NOW TO INF
   JUMP check_summ__rl. 
   /*****/ 
   
   TRANSITION satisfaction__X1/*372*/
   WRITE ready(Null) INTO terminal2queue_ch/*403*/
   FROM NOW TO NOW
   JUMP main__rl.
  END; /* terminal */ 
  
  
 END; /* Client_and_Terminals */ 
/* END; */ /* Banking */  
